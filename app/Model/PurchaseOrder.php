<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class PurchaseOrder extends Model
{
    protected $table = 'po';
    protected $primaryKey = 'po_id';
    public $timestamps = false;
}