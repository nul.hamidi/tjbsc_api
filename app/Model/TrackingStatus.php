<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class TrackingStatus extends Model
{
    protected $table = 'tracking_status';
    protected $primaryKey = 'id';
    public $timestamps = false;
}