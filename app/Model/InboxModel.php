<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class InboxModel extends Model
{
    protected $table = 'inbox';
    protected $primaryKey = 'id';
    public $timestamps = false;
}