<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class TrackingStatusInvoice extends Model
{
    protected $table = 'tracking_status_invoice';
    protected $primaryKey = 'id';
    public $timestamps = false;
}