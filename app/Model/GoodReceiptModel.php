<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class GoodReceiptModel extends Model
{
    protected $table = 'gr';
    protected $primaryKey = 'id';
    public $timestamps = false;
}