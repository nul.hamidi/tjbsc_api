<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\QrCode;

Use Exception;

class QrCodeController extends QrCode
{
    public function scanQrCode(Request $req){
        try{
            //$id = base64_decode($req->input('id'));
            $id = $req->input('id');
            if($req->input('doc')=='pr'){
                $scanning = $this->scanPR($id);
            }else if($req->input('doc')=='po'){
                $scanning = $this->scanPO($id);
            }else if($req->input('doc')=='gr'){
                $scanning = $this->scanGR($id);
            }else{
                $scanning = $this->scanInvoice($id);
            }
            return $this->successResponse("Request berhasil",$scanning,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

    public function scanNumQrCode(Request $req){
        try{
            //$id = base64_decode($req->input('id'));
            $num = $req->input('num');
            if($req->input('doc')=='pr'){
                $scanning = $this->scanPR($num);
            }else if($req->input('doc')=='po'){
                $scanning = $this->scanPO($num);
            }else if($req->input('doc')=='gr'){
                $scanning = $this->scanGR($num);
            }else{
                $scanning = $this->scanInvoice($num);
            }
            return $this->successResponse("Request berhasil",$scanning,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

    public function scanNewQrCode(Request $req){
        try{
            //$id = base64_decode($req->input('id'));
            $id = $req->input('id');
            if($req->input('doc')=='pr'){
                $scanning = $this->scanPR($id);
            }else if($req->input('doc')=='po'){
                $scanning = $this->scanPO($id);
            }else if($req->input('doc')=='gr'){
                $scanning = $this->scanGR($id);
            }else{
                $scanning = $this->scanInvoice($id);
            }
            return $this->successResponse("Request berhasil",$scanning,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }
    public function hitQrCode(Request $request){
        try{
            $link = $request->input();
            
            return $this->successResponse("Request berhasil",$req,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }
    

    public function received(Request $req){

        try{
            $user = auth()->user();
            $token_fcm = $user->token_fcm;
            $id = $req->input('id');
            //$check = $this->checkWorkflowUserExits($req->input('doc'));

            /*if($req->input('doc')=="pr" OR $req->input('doc')=="po"){
                $check = $this->checkOwnerNip($user->nip,$id,$req->input('doc'));
            }else{
                $check = $this->checkWorkflowUserExits($req->input('doc'));
            }*/
            
            $check = $this->checkOwnerNip($user->nip,$id,$req->input('doc'));

            //return $check;
            if($check){
                $scanning = $this->receivedAct($req);
                if($scanning){
                    //$this->sendPushNotification($token_fcm);
                    
                }
                return $this->successResponse("Request berhasil",$scanning,1);
            }else{
                $message = "Sorry you don't have access to receive this document.";
                return $this->errorResponse($message);
            }
            

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }

    }

    public function submit(Request $req){

        try{
            $id = $req->input('id');
            $re = $req->input();

            $user = auth()->user();
            $token_fcm = $user->token_fcm;
            $nip = "112020";
            $payload = response()->json($req);
            $doc = "Submit ".$req->input('doc');
            $this->createLogApi($user->nip,$req->header('token'),$payload,$doc,$req->ip());

            /*if($req->input('doc')=="pr" OR $req->input('doc')=="po"){
                $check = $this->checkOwnerNip($user->nip,$id,$req->input('doc'));
            }else{
                $check = $this->checkWorkflowUserExits($req->input('doc'));
            }*/
            $check = $this->checkOwnerNip($user->nip,$id,$req->input('doc'));
            if($check){
                $message = "Request berhasil";

                $scanning = $this->submited($req,$user);
                if($scanning){
                    
                    //$title="Submit ".strtoupper($req->input('doc'));
                    //$pesan ="";
                    //$this->sendPushNotification($token_fcm);
                    //$this->sendMessage();
                }else{
                    $message = "Document tdk ditemukan";
                }
                return $this->successResponse($message,$scanning,1);
            }else{
                $message = "Sorry you don't have access to receive this document.";
                return $this->errorResponse($message);
            }
            

        }catch (Exception $ex) {
            if($ex->getMessage()=="Object of class stdClass could not be converted to string"){
                $message ="ERROR - workflow#BgCompFailure"; 
            }else{
                $message = $ex->getMessage();
            }
            return $this->errorResponse($message);

        }

    }

    public function submitStaff(Request $req){

        try{
            $staf_id = $req->input('staf_id');

            $user = auth()->user();
            $token_fcm = $user->token_fcm;

            $nip = "112020";
            $payload = response()->json($req);
            $doc = "Submit Staff ".$req->input('doc');
            $this->createLogApi($user->nip,$req->header('token'),$payload,$doc,$req->ip());
            
            $check = $this->checkOwnerNip($user->nip,$req->id,$req->input('doc'));
            if($check){
                $message = "Request berhasil";
                $scanning = $this->submitStaf($req,$user);
                return $this->successResponse($message,$scanning,1);
            }else{
                $message = "Maaf anda belum punya akses untuk submit dokumen ini";
                return $this->errorResponse($message);
            }
            

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }

    }

    public function close(Request $req){

        try{
            $id = $req->input('id');
            $re = $req->input();

            $user = auth()->user();
            $token_fcm = $user->token_fcm;

            $check = $this->checkWorkflowUserExits($req->input('doc'));
            if($check){
                $scanning = $this->closed($req);
                if($scanning){
                    $message = "Request berhasil";
                    //$title="Submit ".strtoupper($req->input('doc'));
                    //$pesan ="";
                    //$this->sendPushNotification($token_fcm);
                    //$this->sendMessage();
                }else{
                    $message = "Document tdk ditemukan";
                }
                return $this->successResponse($message,$scanning,1);
            }else{
                $message = "Maaf anda belum punya akses untuk close dokumen ini";
                return $this->errorResponse($message);
            }
            

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }

    }


}