<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GoodsReceipt;

Use Exception;

class GoodsReceiptController extends GoodsReceipt
{
    public function goodsReceipt(Request $req){
        try{
            $page = @$req->page?@$req->page:1;
            $gr = $this->getGoodsReceipt($page,$req);
            return $this->successResponse("Request berhasil",$gr,count($gr));

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }
}