<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Home;

Use Exception;

class HomeController extends Home
{
    public function homeDashboard(Request $req){
        try{
            
            $home = $this->getDashboard();
            return $this->successResponse("Request berhasil",$home,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }
}