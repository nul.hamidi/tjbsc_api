<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Inbox;

Use Exception;

class InboxController extends Inbox
{
    public function inboxList(Request $req){
        try{
            $page = @$req->page?@$req->page:1;
            $inb = $this->getInboxList($page,$req);
            if(count($inb)){
                $message = "Request berhasil";
            }else{
                $message = "Data Not Found";
            }
            return $this->successResponse($message,$inb);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

    
    public function unreadMessage(Request $req){
        try{
           
            $inb = $this->unreadTotal();
            
            $message = "Request berhasil";
            $data = array(
                'total_unread'=>$inb
            );
          
            return $this->successResponse($message,$data,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

    public function viewMessage(Request $req){
        try{
            $message_id = $req->input('message_id');
            $data = $this->readMessage($message_id);
            
            $message = "Request berhasil";

            return $this->successResponse($message,$data,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

    public function readAllMessage(Request $req){
        try{
            
            $data = $this->readAll();
            
            $message = "Request berhasil";

            return $this->successResponse($message,$data,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }


    


}