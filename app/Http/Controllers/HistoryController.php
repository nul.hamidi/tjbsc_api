<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Historys;

Use Exception;

class HistoryController extends Historys
{
    
    public function historyTraceability(Request $req){

        try{
            $user = auth()->user();
            $token_fcm = $user->token_fcm;
            $payload = response()->json($req);
            $doc = "Detail Document ";
            $this->createLogApi($user->nip,$req->header('token'),$payload,$doc,$req->ip());
            $hisTrace = $this->getHistoryTraceability($req->input());
            
            return $this->successResponse("Request berhasil",$hisTrace,1);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }

    }

    public function ongoing(Request $req){

        try{
            
            $ongoing = $this->ongoingDoc();
            if($ongoing){
                //$total = count($ongoing);
                $total = 1;
            }else{
                $total = 0;
            }
            return $this->successResponse("Request berhasil",$ongoing, $total);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }

    }

    public function historyPR(Request $req){

        try{
            
            $hisPR = $this->getHistoryPR();
            
            return $this->successResponse("Request berhasil",$hisPR,count($hisPR));

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }

    }

    public function historyPO(Request $req){

        try{
            
            $hisPR = $this->getHistoryPO();
            
            return $this->successResponse("Request berhasil",$hisPR,count($hisPR));

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }

    }


}