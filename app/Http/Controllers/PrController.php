<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PurRequest;


Use Exception;
class PrController extends PurRequest
{
    public function purchaseRequests(Request $req){
        try{
            $page = @$req->page?@$req->page:1;
            $pr = $this->getPr($page,$req);
            if(count($pr)){
                $message = "Request berhasil";
            }else{
                $message = "Data Not Found";
            }
            return $this->successResponse($message,$pr);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

    public function createMaximo(Request $req){
    
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);

            $nip = "112020";
            $module = "Create PR ".$req->input('pr_num');
            $payload = response()->json($req);
            $this->createLogApi($nip,$req->header('TJBSCAUTH'),$payload,$module,$req->ip());

            if($auth['success']==true){
                $data = $req->input();
                $message = "Create PR :".$req->input('pr_num')." berhasil";
                $create_pr = $this->createPRMaximo($req);
                return $this->successResponse($message,$data);

            }else{
                return response()->json($auth);
            }

        }catch (Exception $ex) {
            $message = $ex->getMessage() .' in file ' .$ex->getFile().' at line ' .$ex->getLine();
            return $this->errorResponse($message);

        }
        
    }

    public function updateMaximo(Request $req){
    
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);

            $nip = "112020";
            $payload = response()->json($req);
            $this->createLogApi($nip,$req->header('TJBSCAUTH'),$payload,"Update PR",$req->ip());

            //return $req->all();
            if($auth['success']==true){
                $data = $req->input();
                $message = "Update PR :".$req->input('pr_num')." berhasil";
                $create_pr = $this->updatePRMaximo($req);
                return $this->successResponse($message,$data);

            }else{
                return response()->json($auth);
            }

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
        
    }

    public function submitMaximo(Request $req){
    
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);

            $nip = "112020";
            $module = "Submit PR Maximo ".$req->input('pr_num');
            $payload = response()->json($req);
            $this->createLogApi($nip,$req->header('TJBSCAUTH'),$payload,$module,$req->ip());

            //return $auth;
            if($auth['success']==true){
                $data = $req->input();
                $message = "Submit PR:".$req->input('pr_num')." berhasil";
                $create_pr = $this->RoutingPRMaximo($req);
                return $this->successResponse($message,$data);

            }else{
                
                return response()->json($auth);
            }

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
        
    }

    public function listPRMaximo(Request $req){
        
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);
            if($auth['success']==true){

                $page = @$req->page?@$req->page:1;
                $pr = $this->getPrMaximo($page,$req);
                if(count($pr)){
                    $message = "Request berhasil";
                }else{
                    $message = "Data Not Found";
                }
                return $this->successResponse($message,$pr);
                
            }else{
                return response()->json($auth);
            }

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

    


}