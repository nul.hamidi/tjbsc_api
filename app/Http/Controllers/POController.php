<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PurOrder;

Use Exception;

class POController extends PurOrder
{
    public function purchaseOrder(Request $req){
        try{
            $page = @$req->page?@$req->page:1;
            $po = $this->getPOrder($page,$req);
            return $this->successResponse("Request berhasil",$po);

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }
    public function createMaximo(Request $req){
    
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);
            $nip = "112020";
            $module = "Create PO ".$req->input('po_num');
            $payload = response()->json($req);
            $this->createLogApi($nip,$req->header('TJBSCAUTH'),$payload,$module,$req->ip());

            if($auth['success']==true){
                $data = $req->input();
                $message = "Create PO:".$req->input('po_num')." berhasil";
                $create_pr = $this->createPOMaximo($req);
                return $this->successResponse($message,$data);

            }else{
                return response()->json($auth);
            }

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
        
    }

    public function updateMaximo(Request $req){
    
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);
            $nip = "112020";
            $payload = response()->json($req);
            $this->createLogApi($nip,$req->header('TJBSCAUTH'),$payload,"Update PO",$req->ip());

            if($auth['success']==true){
                $data = $req->input();
                $message = "Update PO:".$req->input('pr_num')." berhasil";
                $create_pr = $this->updatePOMaximo($req);
                return $this->successResponse($message,$data);

            }else{
                return response()->json($auth);
            }

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
        
    }

    public function submitMaximo(Request $req){
    
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);
            $nip = "112020";
            $module = "Submit PO Maximo ".$req->input('po_num');
            $payload = response()->json($req);
            $this->createLogApi($nip,$req->header('TJBSCAUTH'),$payload,$module,$req->ip());

            //return $auth;
            if($auth['success']==true){
                $data = $req->input();
                $message = "Submit PO:".$req->input('po_num')." berhasil";
                $create_pr = $this->RoutingPOMaximo($req);
                return $this->successResponse($message,$data);

            }else{
                
                return response()->json($auth);
            }

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
        
    }

    public function listPOMaximo(Request $req){
       
        try{
            $auth = base64_decode($req->header('TJBSCAUTH'));
            $auth = $this->authenticate($auth);

            //return $auth;
            if($auth['success']==true){
                $page = @$req->page?@$req->page:1;
                $po = $this->getPOrderMaximo($page,$req);
                return $this->successResponse("Request berhasil",$po);
            }else{
                    
                return response()->json($auth);
            }

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }

}