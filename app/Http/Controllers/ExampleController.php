<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

Use Exception;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // jika mau menerapkan middleware hanya pada method getUser
        //$this->middleware('age',['only'=>['getUser']]);
        
        // jika mau menerapkan middleware pada semua method kecuali getUser
        //$this->middleware('age',['except'=>['getUser']]);

    }

    public function generateKey(){
        return str::random(32);
    }

    public function fooExample(){
        return "ini adalah foo";
    }

    public function serverIp(Request $req){
        $server = response()->json(
            [
                "ip"=>$req->ip(),
                "ip"=>$req->ip(),
                "server"=>$_SERVER['HTTP_USER_AGENT'],
                "server_" => $req->server('HTTP_USER_AGENT')
            ]);
        
        try {

            return $sqlBean = DB::table("ktv_supplychain_bean_type AS a")
            ->leftJoin("ref_bean_type AS b","a.BeanTypeID","=","b.BeanTypeID")
            ->select("b.BeanTypeID","b.BeanTypeName")
            ->where("a.SupplychainID",$get->SupplychainID)
            ->get();

        } catch (Exception $ex) {
             $ex->getMessage();
        }

        return $server;
    }

    
    public function testAPi(Request $request){
        $arr_data=file_get_contents('PHP://input');
        $att=json_decode($arr_data,true);

        $data = array(
            'by_request' => $request->input(),
            'by_get_content' => $arr_data
        );
        return $data;
    }

    public function getUser($id){
        return "user : ".$id;
    }
    function createLogResponseMaximo($nip,$module,$response){
        $data = array(
            'nip'           => $nip,
            'module'        => $module,
            'response'      => $response,
            'date'          => date('Y-m-d H:i:s'),
        );
        DB::table('log_response_maximo')->insert($data);
    }
    public function getPr(){
        $this->createLogResponseMaximo('1',"submit po",'1');
        sleep(60);
        $this->createLogResponseMaximo('2',"submit po",'2');
        return "sukses";
    }

    public function readApi(Request $request){
        $token = "QklpVWxFc09vRGxtd3ZkQ1FPNVZETG1QWnRFRFdGdml3MkFBY3lZaWk3dDFCbVExYWo=";
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json;');
        header('token:'.$token);
        //$url = "http://inshitech.net/v1/public/api/test-data";
        $url = "http://inshitech.net/v1/public/api/pr?search=&page=1&limit=20";
        $response = file_get_contents($url);
        $json = json_decode($response);
        return response()->json($json->data);
        
    }

    public function readApipr(Request $request){

        $token = "QklpVWxFc09vRGxtd3ZkQ1FPNVZETG1QWnRFRFdGdml3MkFBY3lZaWk3dDFCbVExYWo=";
        $url = "http://inshitech.net/v1/public/api/pr?search=&page=1&limit=20";
        $headers = array
        (
            'Access-Control-Allow-Origin: *',
            'Content-Type: application/json',
            'token: ' . $token
            
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
        $data = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($data);
        return response()->json($json);
        //return response()->json($data);
        
    }

    public function readApiMaximo(Request $request){
        //10.14.9.39:9080/maximo
        $token = "bWF4YWRtaW46bWF4YWRtaW4=";
        //$url = "http://10.14.9.37/maximo/oslc/os/cxprtjbsc?lean=1&oslc.select=spi:prid,spi:prnum,spi:description,spi:status&oslc.where=spi:prnum='22136'";
        $url ='http://10.14.9.37:9080/maximo/oslc/os/cxprtjbsc?lean=1&oslc.select=prid,prnum,description,status';
        $headers = array
        (
            'Access-Control-Allow-Origin: *',
            'Content-Type: application/json',
            'MAXAUTH: ' . $token
            
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
        $data = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($data);
        return response()->json($json);
        // if($json){
        //     $return = array(
        //         'success' => true,
        //         'message'   => "",
        //         'data'  => $json
        //     );
        //     return response()->json($return);
        // }else{
        //     $return = array(
        //         'success' => false,
        //         'message'   => "Data not found",
        //         'data'  => []
        //     );
        //     return response()->json($return);
        // }
        
    }

    public function readApiMaximoPLN(Request $request){

        $token = "bWF4YWRtaW46bWF4YWRtaW4=";
        //$url = "http://10.14.9.37/maximo/oslc/os/cxprtjbsc?lean=1&oslc.select=spi:prid,spi:prnum,spi:description,spi:status&oslc.where=spi:prnum=%2222136%22";
        $url ='http://10.14.9.37:9080/maximo/oslc/os/cxprtjbsc?lean=1&oslc.select=prid,prnum,description,status';
        $headers = array
        (
            'Access-Control-Allow-Origin: *',
            'Content-Type: application/json',
            'MAXAUTH: ' . $token
            
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
        $data = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($data);
        if($json){
            $return = array(
                'success' => true,
                'message'   => "",
                'data'  => $json->member
            );
            return response()->json($return);
        }else{
            $return = array(
                'success' => false,
                'message'   => "Data not found",
                'data'  => []
            );
            return response()->json($return);
        }
        
        //return response()->json($data);
        
    }

    function ConfigPushNotification($fields = array())
    {
        
        $API_ACCESS_KEY = 'AAAAElZZiw8:APA91bGNsQ6xL9wvHS2Wm-1PEK9ADoRJhmUy3ujClEE91TRCvQ19xYUYxDgNMEhMbpS_irwaNKBeuK9L3Rf4479aT9idxTOBi_XofxMgo_DS_7eUHoJXGrk45vnToWewFBH6vLZXMlPe';
        $headers = array
        (
            'Access-Control-Allow-Origin: *',
            'Content-Type: application/json',
            'token: ' . $API_ACCESS_KEY
            
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    }

    public function userTjbps(){
     
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://10.14.9.37:9080/maximo/oslc/os/cxperson?lean=1&oslc.select=personid,displayname,primaryemail,title',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'MAXAUTH: bWF4YWRtaW46bWF4YWRtaW4=',
            'Cookie: JSESSIONID=0000qUE23ndKK_nxHuzZf-1h1iN:-1'
        ),
        ));

        $response = curl_exec($curl);
        $json = json_decode($response);
        curl_close($curl);
        //echo $response; 
        return response()->json($json);
    
    }

    public function testSubmit(){

        $curl = curl_init();

        $data= array(
            'pr_num' => '77777',
            'budget_categories' => 'Planned Outage Spare Parts',
            'asset' => 'Boiler & Auxiliaries',
            'description' => 'HOT & INTEMERDIATE LAYER AIR HEATER BASKET UNIT 1 A&B',
            'status' => 'Created',
            'position' => 'Officer',
            'priority' => '1',
            'total_cost' => '18619605329.84',
            'ownerid' => '1',
            'ownernip' => '80209354',
            'required_date' => '2021-02-07',
            'created_by' => '80209354'
        );

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://10.47.0.20/v1/public/api/pr/create',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            'TJBSCAUTH: MTEyMDIwQHRqYnNjMjAyMA'
        ),
        ));

        $response = curl_exec($curl);
        $json = json_decode($response);
        curl_close($curl);
        return response()->json($json);    

    }

    

    //
}
