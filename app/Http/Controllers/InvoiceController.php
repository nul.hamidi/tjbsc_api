<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\InvoiceRepo;

Use Exception;

class InvoiceController extends InvoiceRepo
{
    public function invoice(Request $req){
        try{
            $page = @$req->page?@$req->page:1;
            $invo = $this->getInvoice($page,$req);
            if(count($invo)){
                $message = "Request berhasil";
            }else{
                $message = "Data Not Found";
            }
            return $this->successResponse($message,$invo,count($invo));

        }catch (Exception $ex) {

            return $this->errorResponse($ex->getMessage());

        }
    }
}