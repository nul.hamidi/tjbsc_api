<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Model\User;
use App\Model\SysUser;
use App\Repositories\Api;
Use Exception;
class AuthController extends Api
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function register(Request $req){
        

        $name = $req->input('name');
        $email = $req->input('email');
        $password = Hash::make($req->input('password'));
        
        // $register = SysUser::create([
        //     'name' => $name,
        //     'nip' => @$req->input('nip'),
        //     'email' => $email,
        //     'password' => $password,
        // ]);
        
        $data = array(
            'name' => $name,
            'nip' => @$req->input('nip'),
            'email' => $email,
            'password' => $password,
        );
        $register = DB::table('sys_users')->insert($data);

        if($register){
            return $this->successResponse("Register Success",$data,1);
            // return response()->json([
            //     'success' =>true,
            //     'message' => 'Register Success',
            //     'data'  =>$register
            // ],200);
        }else{
            return $this->errorResponse("Register Failed");
            // return response()->json([
            //     'success' =>false,
            //     'message' => 'Register Failed',
            //     'data'  =>''
            // ],400);
        }
    }
    public function login(Request $req){
        
        try{
            $username = $req->input('username');
            $password = $req->input('password');

            $site = $req->input('site');
            $payload = response()->json($req);
            $doc = "login ";
            $this->createLogApi($username,$site,$payload,$doc,$req->ip());

            $user = SysUser::where('nip',$username)->first();
        
            if(Hash::check($password, $user->password)){
                $apiToken = base64_encode(str::random(50));

                SysUser::where('nip',$username)->update([
                    'remember_token'    => $apiToken,
                    'token_fcm'         => $req->input('token_fcm'),
                ]);
                $user['token'] = $apiToken;

                $user['role']  = $this->roleUser($user->nip);
                //$user['token_fcm'] = $req->input('token_fcm');
                // return response()->json([
                //     'success' =>true,
                //     'message' => 'Login Success',
                //     'data'  => $user
                // ],200);
                return $this->successResponse("Login berhasil",$user,1);
            }
            // return response()->json([
            //     'success' => false,
            //     'message' => 'Login Failed',
            //     'data'  =>''
            // ],400);
            return $this->errorResponse("password yang anda masukkan salah");
        }catch (Exception $ex){
            return $this->errorResponse("username tidak terdaftar");
        }
    }

    public function loginMaximo(Request $req){
        
        try{
            $username = $req->input('username');
            $password = $req->input('password');
            $site = $req->input('site');

            $payload = response()->json($req);
            $doc = "login ";
            $this->createLogApi($username,$site,$payload,$doc,$req->ip());

            if(strtolower($site)=='pln' OR strtolower($site)=='plntjb' OR strtolower($site)=='-1'){
                $site ="PLNTJB";
            }else{
                $site ="MAXSITE";
            }
            $token = $username.":".$password;
            $MAXAUTH = base64_encode($token);
            $auth = $this->authcUrlMaximo($username,$site,$MAXAUTH);
            //return response()->json($auth);
            if(count(@$auth->member)>0){
                $nip = $auth->member[0]->personid;
                $user = SysUser::where('nip',$nip)->where('site',$site)->first();

                $apiToken = base64_encode(str::random(50));
                $password_hash = Hash::make($password);
                $data = array(
                    'name'              => @$auth->member[0]->displayname,
                    'nip'               => $nip,
                    'email'             => @$auth->member[0]->primaryemail,
                    'password'          => $password_hash,
                    'exp'               => base64_encode($password),
                    'dpass'             => hash('sha512',md5($password)),
                    'position'          => @$auth->member[0]->title?@$auth->member[0]->title:"-",
                    'site'              => $site,
                    'remember_token'    => $apiToken,
                    'token_fcm'         => $req->input('token_fcm'),
                );
                if($user){
                    SysUser::where('nip',$nip)->where('site',$site)->update($data);
                }else{
                    SysUser::insert($data);
                }
                
                $user = SysUser::where('nip',$nip)->where('site',$site)->first();
                $user['token'] = $apiToken;

                $user['role']  = $this->roleUser($user->nip);
               
                return $this->successResponse("Login berhasil",$user,1);
           
                //return response()->json($user->member[0]);
            }else{
                return $this->errorResponse("username atau password salah");
            }
            
        }catch (Exception $ex){
            return $this->errorResponse("username tidak terdaftar");
        }
    }

    public function authcUrlMaximo($login_id,$site,$token){
       
        if($site=="PLNTJB"){
            // site pln
            $url = ENV('IP_PLN')."/maximo/oslc/os/cxperson?lean=1&oslc.select=spi:personid,spi:displayname,spi:primaryemail,spi:title,spi:maxuser%7Bspi:loginid,spi:status,spi:password%7D&oslc.where=spi:maxuser%7Bspi:loginid=%22".$login_id."%22%7D&oslc.where=spi:maxuser%7Bspi:defsite=%22".$site."%22%7D";
        }else{
            // site tjbps
            $url = ENV('IP_TJBPS')."/maximo/oslc/os/cxperson?lean=1&oslc.select=spi:personid,spi:displayname,spi:primaryemail,spi:title&oslc.where=spi:maxuser%7Bspi:loginid=%22".$login_id."%22%7D";
            //$url = "http://10.14.9.37:9080/maximo/oslc/os/cxperson?lean=1&oslc.select=personid,displayname,primaryemail,title&oslc.where=maxuser%7Bloginid=%22".$login_id."%22%7D";
            //$url = "http://10.14.9.37:9080/maximo/oslc/os/cxperson?lean=1&oslc.select=spi:personid,spi:displayname,spi:primaryemail,spi:title,spi:maxuser%7Bspi:loginid,spi:status,spi:password%7D&oslc.where=spi:maxuser%7Bspi:loginid=%22".$login_id."%22%7D&oslc.where=spi:maxuser%7Bspi:defsite=%22".$site."%22%7D";
        }
        
        $headers = array
        (
            'Access-Control-Allow-Origin: *',
            'Content-Type: application/json',
            'MAXAUTH: '.$token
            
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        //curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
        $data = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($data);
        
        return $json;
        //return response()->json($data);
        
    }

    public function authmax(){

         //if($site=="PLNTJB"){
            // site pln
            $url = "http://10.47.0.33/maximo/oslc/os/cxperson?lean=1&oslc.select=spi:personid,spi:displayname,spi:primaryemail,spi:title,spi:maxuser%7Bspi:loginid,spi:status,spi:password%7D&oslc.where=spi:maxuser%7Bspi:loginid=%228510219Z%22%7D&oslc.where=spi:maxuser%7Bspi:defsite=%22PLNTJB%22%7D";
        //}else{
            // site tjbps
            //$url = "http://10.47.0.33/maximo/oslc/os/cxperson?lean=1&oslc.select=spi:personid,spi:displayname,spi:primaryemail,spi:loginid,spi:defsite,spi:title,spi:maxuser{spi:loginid,spi:status,spi:password}&oslc.where=spi:maxuser{spi:loginid=".$login_id."}&oslc.where=spi:maxuser{spi:defsite=".$site."}";
        //}

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'MAXAUTH: ODUxMDIxOVo6ODUxMDIxOVo=',
            'Cookie: JSESSIONID=00009zfRjbnBWvjA9v_5TckaXJQ:-1'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $json = json_decode($response);
        return $json;
    }

    public function changePassword(Request $req){
        $user = auth()->user();
        $password_old = $req->password_old;
        $password_new = $req->input('password_new');

        $user = SysUser::where('user_id',$user->user_id)->first();

        if(Hash::check($password_old, $user->password)){
            $password = Hash::make($password_new);
            SysUser::where('user_id',$user->user_id)->update([
                'password'    => $password,
            ]);

             return $this->successResponse("Change Password Success",$user,1);
        }
        else{
            return $this->errorResponse("Change Password Failed",$user);
        }
    }
}
