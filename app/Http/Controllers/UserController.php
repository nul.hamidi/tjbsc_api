<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Exception;
use App\Repositories\User;
use App\Model\SysUser;

class UserController extends User
{
    public function userList(){
        try {
            $user = $this->indexUser();
            return $this->successResponse("Request berhasil",$user,count($user));
        } catch (Exception $ex) {
            return $this->errorResponse($ex->getMessage());
        }
    }

    public function userProfile(){
        try {
            $user = auth()->user();
            $user['role'] = $this->roleUser($user->nip);
            return $this->successResponse("Request berhasil",$user,1);
        } catch (Exception $ex) {
            return $this->errorResponse($ex->getMessage());
        }
    }


}