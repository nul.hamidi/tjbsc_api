<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\TrackingStatus;
use App\Model\TrackingStatusInvoice;
use App\Repositories\Api;
use App\Repositories\PurRequest;
use App\Repositories\PurOrder;
use App\Repositories\InvoiceRepo;


class Historys extends Api{

    public function getPRByID($id){
        $result = DB::table("pr")
        ->select('pr_id as id','pr_num as num','description AS title','routing_instruction as instruction','memo','status','doc_status','doc_position','received_date','submited_date','date_created as created_date')
                ->where('pr_id',$id)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getPOByID($id){
        $result = DB::table("po")
        ->select('po_id as id','po_num as num','description AS title','routing_instruction as instruction','memo','status','doc_status','doc_position','received_date','submited_date','created_date')
                ->where('po_id',$id)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function traceable_document($id,$doc){

        if($doc=='invoice'){
            $result = TrackingStatusInvoice::select(DB::raw('IFNULL(position,"-") as position'),'received_date','submited_date','status')
            ->leftJoin('sys_users as u','u.nip','=','tracking_status_invoice.user_nip')    
            ->where('invoice_id',$id)
                ->orderBy('id','DESC')
                ->get();
        }else{
            $result = TrackingStatus::select(DB::raw('IFNULL(u.name,IFNULL(workflow,user_nip)) as position'),'sent_date','received_date','submited_date','status')
                ->leftJoin('sys_users as u','u.nip','=','tracking_status.user_nip')
                ->where('source_id',$id)
                ->where('source_doc',$doc)
                ->whereNotNull('user_nip')
                ->orderBy('id','DESC')
                ->get();
        }
        

       
        return $result->map(function($res){
           
            $res['received_date'] = $res->sent_date?date('H:i d/m/Y',strtotime($res->sent_date)):"";
            $res['submited_date'] = $res->submited_date?date('H:i d/m/Y',strtotime($res->submited_date)):"";
            $res['position']      = $res->position?$res->position:'-';
            
            return $res;
         });
         //return  $result;
        
    }

    public function getHistoryTraceability($req){
        $id  = $req['id'];
        $doc = $req['doc'];
        $data = array();
        if($doc=='pr'){
            $pr = $this->getPRByID($id);
            $tracking = $this->traceable_document($id,$doc);
            if($pr){
                $data['id'] = $pr->id;
                $data['num'] = $pr->num;
                $data['title'] = $pr->title;
                $data['instruction'] = $pr->instruction?$pr->instruction:"Please review this PR";
                $data['memo'] = $pr->memo?$pr->memo:"-";
                $data['status'] = $pr->doc_status;
                $data['created_date'] = date('d M Y',strtotime($pr->created_date));
                
            }
            

        }else if($doc=='po'){
            $po = $this->getPOByID($id);
            $tracking = $this->traceable_document($id,$doc);
            if($po){
                $data['id'] = $po->id;
                $data['num'] = $po->num;
                $data['title'] = $po->title;
                $data['instruction'] = $po->instruction?$po->instruction:"Please review this PO";
                $data['memo'] = $po->memo?$po->memo:"-";
                $data['status'] = $po->doc_status;
                $data['created_date'] = date('d M Y',strtotime($po->created_date));
                
            }
           
        }
        else if($doc=='gr'){
            $GoodReceipt = new GoodsReceipt();
            $gr = $GoodReceipt->getGRByID($id);
            $tracking = $this->traceable_document($id,$doc);
            if($gr){
                $data['id'] = $gr->id;
                $data['num'] = $gr->num;
                $data['title'] = $gr->title;
                $data['status'] = $gr->doc_status;
                $data['instruction'] = $gr->instruction?$gr->instruction:"Please Review this Goods Receipt";
                $data['memo'] = $gr->memo?$gr->memo:"-";
                $data['created_date'] = date('d M Y',strtotime($gr->created_date));
            }
        
        }
        else if($doc=='invoice'){
            $invoice = new InvoiceRepo();
            $inv = $invoice->getInvoiceByID($id);
            $tracking = $this->traceable_document($id,$doc);
            if($inv){
                $data['id'] = $inv->id;
                $data['num'] = $inv->num;
                $data['title'] = $inv->title;
                $data['status'] = $inv->status;
                $data['instruction'] = $inv->instruction?$gr->instruction:"Please Review this Invoice";
                $data['memo'] = $inv->memo?$inv->memo:"-";
                $data['created_date'] = date('d M Y',strtotime($inv->created_date));
            }
        
        }
        else{
            $data['id'] = "";
            $data['num'] = "";
            $data['title'] = "";
            $data['doc_status'] = "";
        }

        $data['tracking'] = $tracking;

        return $data;

    }

    public function ongoingDoc(){
        $return = array();
        $user = auth()->user();
        $nip = $user->nip;
        //$pr = DB::table('tracking_status');
        $result_pr = DB::select("SELECT
                        pr.pr_id as id,
                        pr.doc,
                        wk.doc wkdoc,
                        pr.pr_num as num,
                        pr.description as title,
                        pr.routing_instruction as routing_instruction,
                        pr.status as doc_status,
                        pr.doc_position,
                        IFNULL(u.name,pr.doc_position) position,
                        pr.sent_date,
                        now() as now,
                        pr.received_date,
                        pr.submited_date,
                        pr.date_created created_date,
                        IFNULL(wk.baku_waktu,3) baku_waktu,
                        ROUND(time_to_sec((TIMEDIFF(now(),pr.sent_date ))) / 60) menit,
                        DATEDIFF(now(),IFNULL(pr.sent_date,pr.date_created)) as days	
                    FROM
                        pr
                        LEFT JOIN sys_users as u ON u.nip=pr.process_owner_nip
                        LEFT JOIN workflow_user as wu ON wu.user_nip=pr.process_owner_nip
                        LEFT JOIN workflow as wk ON wk.workflow_id=wu.workflow_id
                        
                    WHERE
                        pr.process_owner_nip = ?
                        AND pr.doc='pr'
                    group by pr.pr_id
            ",[$nip]);

        $result_po = DB::select("SELECT
            po.po_id as id,
            po.doc,
            wk.doc wkdoc,
            po.po_num as num,
            po.description as title,
            po.routing_instruction as routing_instruction,
            po.status as doc_status,
            po.doc_position,
            IFNULL(u.name,po.doc_position) position,
            po.sent_date,
            now() as now,
            po.received_date,
            po.submited_date,
            po.created_date created_date,
            IFNULL(wk.baku_waktu,3) baku_waktu,
            ROUND(time_to_sec((TIMEDIFF(now(),po.sent_date ))) / 60) menit,
            DATEDIFF(now(),IFNULL(po.sent_date,po.created_date)) as days	
            FROM
            po
            LEFT JOIN sys_users as u ON u.nip=po.process_owner_nip
            LEFT JOIN workflow_user as wu ON wu.user_nip=po.process_owner_nip
            LEFT JOIN workflow as wk ON wk.workflow_id=wu.workflow_id

            WHERE
            po.process_owner_nip = ?
            AND po.doc='po'
            group by po.po_id
            ",[$nip]);

        $result_gr = DB::select("SELECT
                gr.id,
                gr.doc,
                wk.doc wkdoc,
                gr.po_num as num,
                gr.description as title,
                gr.routing_instruction as routing_instruction,
                gr.doc_status,
                gr.doc_position,
                u.position,
                gr.sent_date,
                now() as now,
                gr.received_date,
                gr.submited_date,
                gr.created_date created_date,
                IFNULL(wk.baku_waktu,3) baku_waktu,
                ROUND(time_to_sec((TIMEDIFF(now(),gr.sent_date ))) / 60) menit,
                DATEDIFF(now(),IFNULL(gr.sent_date,gr.created_date)) as days	
                FROM
                gr
                LEFT JOIN sys_users as u ON u.nip=gr.process_owner_nip
                LEFT JOIN workflow_user as wu ON wu.user_nip=gr.process_owner_nip
                LEFT JOIN workflow as wk ON wk.workflow_id=wu.workflow_id
                
                WHERE
                gr.process_owner_nip = ?
                AND gr.doc='gr'
                group by gr.id
         ",[$nip]);


        $result_invoice = DB::select("SELECT
            ts.invoice_id id,
            'invoice' doc,
            inv.num,
            inv.text title,
            inv.routing_instruction,
            ts.status as doc_status,
            ts.workflow as position,
            ts.sent_date,
            now() as now,
            ts.received_date,
            ts.submited_date,
            wk.baku_waktu,
            inv.created_date,
            ROUND(time_to_sec((TIMEDIFF(IFNULL(ts.submited_date,now()),IFNULL(ts.received_date,ts.sent_date) ))) / 60) menit,	
            DATEDIFF(now(),IFNULL(ts.sent_date,inv.created_date)) as days
            FROM
            tracking_status_invoice ts
            LEFT JOIN workflow wk ON wk.workflow_id=ts.workflow_id
            LEFT JOIN invoice inv ON inv.id=ts.invoice_id 
            WHERE
            ts.user_nip = ?
            AND ( ts.received_date IS NULL OR ts.submited_date IS NULL)
            ORDER BY ts.id DESC
            ",[$nip]);

        if(count($result_pr) OR count($result_po) OR count($result_gr) OR count($result_invoice)){
            $total = count($result_pr)+count($result_po)+count($result_gr)+count($result_invoice);
            $return = array();
            if(count($result_pr)>0){
                foreach ($result_pr as $key) {
                    /*$sent = $key->received_date?$key->received_date:$key->sent_date;
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $sent);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);
                    $sisa_waktu = (int) ($diff_in_days+1)-$key->baku_waktu;*/
                    $sisa_waktu = (int) ($key->days)-$key->baku_waktu;
                
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = "On-Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#f0f000";//biru
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//biru
                    }
                    $return[] = array(
                        'id'            => $key->id,
                        'num'           => $key->num,
                        'doc'           => $doc,
                        'title'         => $key->title,
                        'doc_status'    =>  $key->doc_status,
                        'doc_position'  =>  $key->position,
                        'instruction'   =>  $key->routing_instruction?$key->routing_instruction:"Please review this ".$doc,
                        'sent_date'     => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'received_date'  => $key->received_date?date('d M Y',strtotime($key->received_date)):"-",
                        'submited_date'  => $key->submited_date?$key->submited_date:"",
                        'created_date'       => $key->created_date?date('d M Y',strtotime($key->created_date)):"",
                        'verification_time' => $key->days,
                        'warna'          => $color,
                    );
                    # code...
                }
            }
            if(count($result_po)>0){
                foreach ($result_po as $key) {
                    
                    $sisa_waktu = (int) ($key->days)-$key->baku_waktu;
                
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = "On-Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#f0f000";//biru
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//biru
                    }
                    $return[] = array(
                        'id'            => $key->id,
                        'num'           => $key->num,
                        'doc'           => $doc,
                        'title'         => $key->title,
                        'doc_status'    =>  $key->doc_status,
                        'doc_position'  =>  $key->position,
                        'instruction'   =>  $key->routing_instruction?$key->routing_instruction:"Please review this ".$doc,
                        'sent_date'     => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'received_date'  => $key->received_date?date('d M Y',strtotime($key->received_date)):"-",
                        'submited_date'  => $key->submited_date?$key->submited_date:"",
                        'created_date'       => $key->created_date?date('d M Y',strtotime($key->created_date)):"",
                        'verification_time' => $key->days,
                        'warna'          => $color,
                    );
                    # code...
                }
            }
            if(count($result_gr)>0){
                foreach ($result_gr as $key) {

                    $sisa_waktu = (int) ($key->days)-$key->baku_waktu;
                
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = "On-Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#f0f000";//biru
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//biru
                    }
                    $return[] = array(
                        'id'            => $key->id,
                        'num'           => $key->num,
                        'doc'           => $doc,
                        'title'         => $key->title,
                        'doc_status'    =>  $key->doc_status,
                        'doc_position'  =>  $key->position,
                        'instruction'   =>  $key->routing_instruction?$key->routing_instruction:"Please review this ".$doc,
                        'sent_date'     => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'received_date'  => $key->received_date?date('d M Y',strtotime($key->received_date)):"-",
                        'submited_date'  => $key->submited_date?$key->submited_date:"",
                        'created_date'       => $key->created_date?date('d M Y',strtotime($key->created_date)):"",
                        'verification_time' => $key->days,
                        'warna'          => $color,
                    );
                    # code...
                }
            }
            if(count($result_invoice)>0){
                foreach ($result_invoice as $key) {
                    /*$sent = $key->received_date?$key->received_date:$key->sent_date;
                    //$receipt = "2020-10-20 10:10:10";
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $sent);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);*/
                    $sisa_waktu = (int) ($key->days)-$key->baku_waktu;
                
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = "On-Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#f0f000";//biru
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//biru
                    }
                    $return[] = array(
                        'id'        => $key->id,
                        'num'       => $key->num,
                        'doc'       => $doc,
                        'title'     => $key->title,
                        'doc_status'    =>  $key->doc_status,
                        'doc_position'  =>  $key->position,
                        'instruction'   =>  $key->routing_instruction,
                        'sent_date'     => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'received_date'  => $key->received_date?date('d M Y',strtotime($key->received_date)):"-",
                        'submited_date'  => $key->submited_date?$key->submited_date:"",
                        'created_date'       => $key->created_date?date('d M Y',strtotime($key->created_date)):"",
                        'verification_time' => $key->days,
                        'warna'          => $color,
                    );
                    # code...
                }
            }
        }else{
            $return = [];
            $total = 0;
        }
        

        return $return;
    }
    public function ongoingDocOld(){
        $return = array();
        $user = auth()->user();
        $nip = $user->nip;
        //$pr = DB::table('tracking_status');
       $result = DB::select("SELECT
                ts.source_id as id,
                ts.source_doc as doc,
                IFNULL(pr.pr_num,IFNULL(po.po_num,gr.po_num)) num,
                IFNULL(pr.description,IFNULL(po.description,gr.Description)) title,
                IFNULL(pr.routing_instruction,IFNULL(po.routing_instruction,gr.routing_instruction)) routing_instruction,
                ts.status as doc_status,
                ts.workflow as doc_position,
                ts.sent_date,
                now() as now,
                ts.received_date,
                ts.submited_date,
                IFNULL(pr.date_created,IFNULL(po.created_date,gr.created_date)) created_date,
                wk.baku_waktu,
                ROUND(time_to_sec((TIMEDIFF(IFNULL(ts.submited_date,now()),IFNULL(ts.received_date,ts.sent_date) ))) / 60) menit	
            FROM
                tracking_status ts
                LEFT JOIN workflow wk ON wk.workflow_id=ts.workflow_id
                LEFT JOIN pr ON pr.pr_id=ts.source_id AND source_doc='pr'
                LEFT JOIN po ON po.po_id=ts.source_id AND source_doc='po'
                LEFT JOIN gr ON gr.id=ts.source_id AND source_doc='gr'
            WHERE
                ts.user_nip = ?
                AND ts.received_date IS NOT NULL 
                AND ts.submited_date IS NULL
            ",[$nip]);

        $result_invoice = DB::select("SELECT
            ts.invoice_id id,
            'invoice' doc,
            inv.num,
            inv.text title,
            inv.routing_instruction,
            ts.status as doc_status,
            ts.workflow as doc_position,
            ts.sent_date,
            now() as now,
            ts.received_date,
            ts.submited_date,
            wk.baku_waktu,
            inv.created_date,
            ROUND(time_to_sec((TIMEDIFF(IFNULL(ts.submited_date,now()),IFNULL(ts.received_date,ts.sent_date) ))) / 60) menit	
            FROM
            tracking_status_invoice ts
            LEFT JOIN workflow wk ON wk.workflow_id=ts.workflow_id
            LEFT JOIN invoice inv ON inv.id=ts.invoice_id 
            WHERE
            ts.user_nip = ?
            AND ( ts.received_date IS NULL OR ts.submited_date IS NULL)
            ORDER BY ts.id DESC
            ",[$nip]);

        if(count($result) OR count($result_invoice)){
            $total = count($result);
            if(count($result)>0){
                foreach ($result as $key) {
                    $sent = $key->received_date?$key->received_date:$key->sent_date;
                    //$receipt = "2020-10-20 10:10:10";
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $sent);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);
                    $sisa_waktu = (int) ($diff_in_days+1)-$key->baku_waktu;
                
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = "On-Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#f0f000";//biru
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//biru
                    }
                    $return[] = array(
                        'id'            => $key->id,
                        'num'           => $key->num,
                        'doc'           => $doc,
                        'title'         => $key->title,
                        'doc_status'    =>  $key->doc_status,
                        'doc_position'  =>  $key->doc_position,
                        'instruction'   =>  $key->routing_instruction?$key->routing_instruction:"Please review this ".$doc,
                        'sent_date'     => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'received_date'  => $key->received_date?date('d M Y',strtotime($key->received_date)):"",
                        'submited_date'  => $key->submited_date?$key->submited_date:"",
                        'created_date'       => $key->created_date?date('d M Y',strtotime($key->created_date)):"",
                        'verification_time' => $diff_in_days,
                        'warna'          => $color,
                    );
                    # code...
                }
            }
            if(count($result_invoice)>0){
                foreach ($result_invoice as $key) {
                    $sent = $key->received_date?$key->received_date:$key->sent_date;
                    //$receipt = "2020-10-20 10:10:10";
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $sent);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);
                    $sisa_waktu = (int) ($diff_in_days+1)-$key->baku_waktu;
                
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = "On-Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#f0f000";//biru
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//biru
                    }
                    $return[] = array(
                        'id'        => $key->id,
                        'num'       => $key->num,
                        'doc'       => $doc,
                        'title'     => $key->title,
                        'doc_status'    =>  $key->doc_status,
                        'doc_position'  =>  $key->doc_position,
                        'instruction'   =>  $key->routing_instruction,
                        'sent_date'     => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'received_date'  => $key->received_date?date('d M Y',strtotime($key->received_date)):"",
                        'submited_date'  => $key->submited_date?$key->submited_date:"",
                        'created_date'       => $key->created_date?date('d M Y',strtotime($key->created_date)):"",
                        'verification_time' => $diff_in_days,
                        'warna'          => $color,
                    );
                    # code...
                }
            }
        }else{
            $return = [];
            $total = 0;
        }
        

        return $return;
    }

    public function getHistoryPR(){

        $result = DB::table("pr")
        ->select('pr_id','pr_num','description AS title','status','doc_status','doc_position','received_date','submited_date')
        ->whereNotNull('doc_position')
        ->limit(3)
        ->get();
        $return = array();
        $i=1;
        foreach ($result as $key) {
            if($i==1){
                $received_date= "09:10 16/10/2020";
                $redate= "2020-10-16 10:02:15";
            }else{
                $received_date= "13:10 16/10/2020";
                $redate= "2020-10-16 13:02:15";
            }
            $now = "2020-10-18 08:30:00";
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $redate);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $now);
            $diff_in_days = $to->diffInDays($from);
            $return[] = array(
                'id' => $key->pr_id,
                'num' => $key->pr_num,
                'title' => $key->title,
                'status' => $key->status,
                'doc_status' => $key->doc_status,
                'doc_position' => $key->doc_position,
                'received_date' => $received_date,
                'submited_date' => "",
                'verification_time' => $diff_in_days ." hari", 
                'doc' =>'PR'
            );
            $i++;
        }

        return $return;
    }

    public function getHistoryPO(){

        $result = DB::table("po")
        ->select('po_id','po_num','description AS title','status','doc_status','doc_position','received_date','submited_date')
        ->limit(4)
        ->get();
        $return = array();
        $i=1;
        foreach ($result as $key) {
            if($i==1){
                $received_date= "09:10 16/10/2020";
                $redate= "2020-10-17 10:02:15";
            }else{
                $received_date= "13:10 16/10/2020";
                $redate= "2020-10-16 13:02:15";
            }
            $now = "2020-10-18 08:30:00";
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $redate);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $now);
            $diff_in_days = $to->diffInDays($from);
            $return[] = array(
                'id' => $key->po_id,
                'num' => $key->po_num,
                'title' => $key->title,
                'status' => $key->status,
                'doc_status' => $key->doc_status,
                'doc_position' => $key->doc_position,
                'received_date' => $received_date,
                'submited_date' => "",
                'verification_time' => $diff_in_days ." hari", 
                'doc' =>'PO'
            );
            $i++;
        }

        return $return;
    }

 


    

}

?>