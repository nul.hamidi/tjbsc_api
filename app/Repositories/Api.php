<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\SysUser;
class Api{

    public $settingLog = 3;

    public function settingLogFunc(){
        // $setting_log = 1  => log submit d simpan dalam bentuk file json
        // $setting_log = 2  => log di save ke database
        // $setting_log = 3  => log di save ke database dan save file json
        $setting_log = 1;

        return $setting_log;
    }

    public function createSysLogApi($direktory,$filename,$data){

        if($this->settingLog==1){
            if(!is_dir($direktory)){
                \Storage::makeDirectory($direktory);
            }
            \Storage::disk('local')->put($direktory.'/'.$filename.'.json', $data['Payload']);
        }
        else if($this->settingLog==2){
            DB::table('sys_log_api')->insert($data);
        }
        else{

            if(!is_dir($direktory)){
                \Storage::makeDirectory($direktory);
            }
            \Storage::disk('local')->put($direktory.'/'.$filename.'.json', $data['Payload']);

            DB::table('sys_log_api')->insert($data);
        }
        

        
    }
    
    public function errorValidationResponse($message) {
        return response(400)->json(
            array(
                "success"   => false,
                "message"   => $message,
                "data"      => null,
                "total"     => '0'
            ),400);
    }

    public function successResponse($message="",$data="", $total=1) {
        if ($message == null) {
            $message = "";
        }

        return response()->json(
            [
                "success"       => true,
                "message"       => $message,
                "data"          => $data,
                "total"         => $total
            ],200
        );
    }

    public function errorResponse($message="",$data="",$total=0) {
        if ($message == null) {
            $message = "";
        }

        return response()->json(
            [
                "success"       => false,
                "message"       => $message,
                "data"          => $data,
                "total"         => $total
            ],400
        );
            
    }

    public function notFound($message) {

        return response()->json(array(
            "status"    => 404,
            "message"   => $message,
            "data"      => null
        ),404
        );
    }

    public function getSupplychainID($sid){
        $return = "";

        $result = DB::table("ktv_tc_supplychain_org")
                ->where("SupplychainID",$sid)
                ->first();

        if($result){
            return $result;
        }

        return $return;
    }

    function ConfigPushNotification($fields = array())
    {
        
        $API_ACCESS_KEY = 'AAAAElZZiw8:APA91bGNsQ6xL9wvHS2Wm-1PEK9ADoRJhmUy3ujClEE91TRCvQ19xYUYxDgNMEhMbpS_irwaNKBeuK9L3Rf4479aT9idxTOBi_XofxMgo_DS_7eUHoJXGrk45vnToWewFBH6vLZXMlPe';
        $headers = array
        (
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    }

    function sendPushNotification($token_fcm,$title="",$message=""){

        //$title = 'Receipt Doc';
        //$message = 'PR 007 telah diterima oleh atasan';
        $fields = [
            //'registration_ids'  => ['ddhsZeGh9TA:APA91bGFShkH1P1rs3cT29G0M6NW2VK1AqMOUKtkM-ywtJx7Lb9TqYKAVN-OnM7EMgt6dt56iFeG3qkoONjY2y2zghzYqAOyJ5tzsIMQ4_mp5ef2pGhdVRyEWUrY8FXC4lPpcjGQcN0t','ewwgy6ZE01c:APA91bH-xAcSxuSXoeuKZCSKurlO9kzfqy5VmbgKiseKi8X9OeMfqKMmRXNiK6A7sGwvPxF0F5PAfMhhnA4CsItLUFFO2Fn2cOTQeJAb6ekKj1Vm7tUOyi4yK45OaoQl7Yx_gT8XPq-h'],
            'registration_ids'  => [$token_fcm],
            'data'          => [
                'redirectto' => 'browse',
                'image' => 'https://freeiconshop.com/wp-content/uploads/edd/link-open-flat.png'
            ],
            'priority' => 'high',
            'notification' => [
                'body' => $message,
                'title' => $title,
                'sound' => 'default',
                'icon' => 'https://freeiconshop.com/wp-content/uploads/edd/link-open-flat.png'
            ],
            'android' => [
                'notification' => [
                    'click_action' => 'FCM_PLUGIN_ACTIVITY'
                ]
            ]
        ];
        $user = auth()->user();
        $data = array(
            'token_fcm' => $token_fcm,
            'title'     => $title,
            'message'   => $message,
            'sent_by'   => 112020,
            'created_date'  => date('Y-m-d H:i:s')
        );
        DB::table('sys_log_push_notification')->insert($data);

        $this->ConfigPushNotification($fields);

        
    }

    function createLogApi($nip,$token,$payload,$module,$ip){
        $data = array(
            'username'      => $nip,
            'token'         => $token,
            'url'           => URL::current(''),
            'ip'            => $ip,
            'payload'       => $payload,
            'akses_module'  => $module,
            'date'          => date('Y-m-d H:i:s'),
        );
        DB::table('sys_log_api')->insert($data);
    }

    function sendMessage($data){
        DB::table('inbox')->insert($data);
    }

    function createLogResponseMaximo($nip,$module,$response){
        $data = array(
            'nip'           => $nip,
            'module'        => $module,
            'response'      => $response,
            'date'          => date('Y-m-d H:i:s'),
        );
        DB::table('log_response_maximo')->insert($data);
    }

    function createLogErrorMaximo($nip,$module,$response){
        $data = array(
            'nip'           => $nip,
            'module'        => $module,
            'response'      => $response,
            'date'          => date('Y-m-d H:i:s'),
        );
        DB::table('log_error_maximo')->insert($data);
    }

    function getWorkFlowUserID($id){
        $result = DB::table("workflow_user")
                ->where('workflow_id',$id)
                ->get();

        if($result){
            return $result;
        }
        return false;
    }

    function checkWorkflowUserExits($doc){
        $user = auth()->user();
        $result = DB::table("workflow_user as wu")
                    ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                    ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                    ->where('wu.user_nip',$user->nip)
                    ->where('w.doc',$doc)
                    ->get();
        if(count($result)>0){
            return $result;
        }
        return false;
    }

    public function roleUser($nip){
        //$user = auth()->user();

       $result = DB::table("workflow_user as wu")
                    ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                    ->select('w.workflow_name as workflow','w.doc')
                    ->where('wu.user_nip',$nip)
                    ->groupBy('w.doc')
                    ->get();
        if(count($result)>0){
            return $result;
        }
        return false;
        
        return $result;
    }

    public function authenticate($auth){
        //nip : 112020
        //pass: tjbsc2020
        //base64_encode = MTEyMDIwQHRqYnNjMjAyMA==
        
        try{
            list($username,$password) = explode('@',$auth);

            $user = SysUser::where('nip',$username)->first();
        
            if(Hash::check($password, $user->password)){
                $result = array(
                    'success' => true,
                    'message' => "login successed"
                );
                return $result;
            }
            $result = array(
                'success' => false,
                'message' => "login failed"
            );
            return $result;
        }catch (Exception $ex){
            $result = array(
                'success' => false,
                'message' => "login failed"
            );
            return $result;
        }
    }

    public function checkOwnerNip($nip,$id,$doc){
        if(strtolower($doc)=='pr'){
            $query = DB::table('pr')
            ->where('pr_id',$id)
            ->where('process_owner_nip',$nip)
            ->first();
        }
        else if (strtolower($doc)=='po'){
            $query = DB::table('po')
            ->where('po_id',$id)
            ->where('process_owner_nip',$nip)
            ->first();
        }
        else if (strtolower($doc)=='gr'){
            $query = DB::table('gr')
            ->where('id',$id)
            ->where('process_owner_nip',$nip)
            ->first();
        }
        else if (strtolower($doc)=='invoice'){
            $query = DB::table('invoice')
            ->where('id',$id)
            ->where('process_owner_nip',$nip)
            ->first();
        }
        else{
            $query = false;
        }
        
        return $query;
    }

    public function getUserDisposisi(){

        $user =  auth()->user();
        $token = $user->nip.":".base64_decode($user->exp);
        $MAXAUTH = base64_encode($token);
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => ENV('IP_PLN').'/maximo/oslc/os/cxuser?lean=1&oslc.select=personid,person.displayname&oslc.where=persongroupteam%7Bpersongroup%20in%20%5B%22AEHAR1%22,%22AEHAR2%22,%22AMNHAR12%22%5D%7D',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'MAXAUTH: '.$MAXAUTH,
            //'MAXAUTH: bWF4YWRtaW46bWF4YWRtaW4='
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $json = json_decode($response);
        
        if(@$json->member){
            $user_staf= array();
            foreach (@$json->member as $key) {
                $user_staf[] =array(
                    'staf_id'       => @$key->personid,
                    'staf_title'    => @$key->person->displayname,
                );
            }
            return $user_staf;
        }

        return false;
    }

    
    public function apiSubmitUserDisposisi($prid,$staf_id){
        $user =  auth()->user();
        $token = $user->nip.":".base64_decode($user->exp);
        $MAXAUTH = base64_encode($token);

        $data = array(
            "cxassigncodesent" =>$staf_id
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => ENV('IP_PLN').'/maximo/oslc/os/cxprperson/'.$prid.'?lean=1',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($data),
        CURLOPT_HTTPHEADER => array(
            //'MAXAUTH: bWF4YWRtaW46bWF4YWRtaW4=',
            'MAXAUTH: '.$MAXAUTH,
            'x-method-override: PATCH',
            'patchtype: MERGE',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //echo $response;
        $this->createLogResponseMaximo($user->nip,"submit user disposisi",json_decode($response));

    }

    public function apiSubmitPR($IPADDRESS,$ownerid,$data){
        $user =  auth()->user();
        $token = $user->nip.":".base64_decode($user->exp);
        $MAXAUTH = base64_encode($token);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $IPADDRESS.'/maximo/oslc/os/mxapiwfassignment/'.$ownerid.'?action=wsmethod:completeAssignment',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            //'MAXAUTH: bWF4YWRtaW46SmFrYXJ0YTEyMzQ1',
            'MAXAUTH: '.$MAXAUTH,
            'x-method-override: PATCH',
            'patchtype: MERGE',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //echo $response;
        $this->createLogErrorMaximo($user->nip,"submit pr",$response);
        $this->createLogResponseMaximo($user->nip,"submit pr",json_decode($response));
    }

    public function apiSubmitPO($IPADDRESS,$ownerid,$data){

        $user =  auth()->user();
        $token = $user->nip.":".base64_decode($user->exp);
        $MAXAUTH = base64_encode($token);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $IPADDRESS.'/maximo/oslc/os/mxapiwfassignment/'.$ownerid.'?action=wsmethod:completeAssignment',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($data),
        CURLOPT_HTTPHEADER => array(
            //'MAXAUTH: bWF4YWRtaW46bWF4YWRtaW4=',
            'MAXAUTH: '.$MAXAUTH,
            'x-method-override: PATCH',
            'patchtype: MERGE',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //echo $response;
        $this->createLogErrorMaximo($user->nip,"submit po",$response);
        $this->createLogResponseMaximo($user->nip,"submit po",json_decode($response));
    }




   

  


}

?>