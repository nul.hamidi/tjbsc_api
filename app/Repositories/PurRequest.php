<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\PurchaseRequest;
use App\Repositories\Api;

class PurRequest extends Api{
    
    public function getPr($page,$req) {
        $return = array();
        $limit = @$req->limit?@$req->limit:20;
        $search = @$req->search?@$req->search:"";
        $doc = 'pr';
        $result = PurchaseRequest::
                select('pr.pr_id as id','pr.pr_num as num','pr.description AS title','pr.routing_instruction AS instruction','pr.sent_date','st.received_date','pr.submited_date','pr.date_created as created_date','pr.process_owner_id AS ownerid',DB::raw('IFNULL(u.name,pr.doc_position) position,IFNULL(process_owner_nip,"-") as ownernip,IFNULL(pr.status,st.status) status,IFNULL(pr.doc_status,st.status) doc_status,DATEDIFF(now(),IFNULL(pr.sent_date,pr.date_created)) as days'))
                //->leftJoin(DB::raw('(SELECT source_id,source_doc, status, workflow,received_date, submited_date from `tracking_status` group by id  order by id DESC) AS st'),'st.source_id','=','pr.pr_id')
                ->leftJoin(DB::raw('(SELECT source_id,source_doc, status, workflow,received_date, submited_date from `tracking_status` group by id  order by id ASC) AS st'), function($join) {
                    $join->on('pr.pr_id', '=', 'st.source_id');
                    $join->on('pr.doc', '=', 'st.source_doc');
                })
                ->leftJoin('sys_users as u','u.nip','=','pr.process_owner_nip')
                ->when($search, function ($query, $search) {
                     return $query->where('pr_num','like','%'.$search.'%');
                })
                ->when($search, function ($query, $search) {
                    return $query->orWhere('description','like','%'.$search.'%');
                })
                ->groupBy('pr.pr_id')
                ->orderBy('pr_id','DESC')
                ->paginate($limit,$page);
            //->toSql();
        $result->map(function($res){

            if($res->id=='664737016'){
                $warna = "#dc143c";//merah
            }else if($res->id=='664738723'){
                $warna = "#f0f000";//kuning
            }
            else{
                
                $warna = "#0089C7";//biru
            }
            //$now = date('Y-m-d H:s:i');
            //$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $res->sent_date);
            //$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:', $now);
            //$diff_in_days = $to->diffInDays($from);

            if($res->received_date!=""){
                $warna = "#3cb371";//hijau
            }else{
                $warna = "#0089C7";//biru
            }

            $res['status'] = $res->status?$res->status:"";
            $res['doc_status'] = $res->doc_status?$res->doc_status:"";
            $res['doc_position'] = $res->position?$res->position:$res->ownernip;
            $res['instruction'] = $res->instruction?$res->instruction:"Please review this PR";
            $res['sent_date'] = $res->sent_date?date('d M Y',strtotime($res->sent_date)):"-";
            $res['received_date'] = $res->received_date?$res->received_date:"";
            $res['submited_date'] = $res->submited_date?$res->submited_date:"";
            $res['created_date'] = $res->created_date?date('d M Y',strtotime($res->created_date)):"";
            $res['verification_time'] = $res->days?$res->days:"-";
            $res['status_color'] = $warna;
            $res['doc'] = "pr";
        
            return $res;
        });
        
        $data = array(
            'data'      => $result['data'],
            //'last_page' => $result->last_page,
            //'total'     => $result->total
        );
        return $result;
    }

    public function getPrMaximo($page,$req) {
        $return = array();
        $limit = @$req->limit?@$req->limit:20;
        $search = @$req->search?@$req->search:"";
        $doc = 'pr';
        $result = PurchaseRequest::
                select('pr.pr_id','pr_num','budget_categories','asset','description','status','status_description','doc_position as position','priority','total_cost','remarks','routing_instruction','prid','process_owner_id as ownerid','process_owner_nip as ownernip','required_date','created_by','date_created as created_date')
                
                ->when($search, function ($query, $search) {
                     return $query->where('pr_num','like','%'.$search.'%');
                })
                ->when($search, function ($query, $search) {
                    return $query->orWhere('description','like','%'.$search.'%');
                })
                ->groupBy('pr.pr_id')
                ->orderBy('pr_id','DESC')
                ->paginate($limit,$page);
            //->toSql();
        $result->map(function($res){

            
            // $res['status'] = $res->status?$res->status:"";
            // $res['doc_status'] = $res->doc_status?$res->doc_status:"";
            // $res['doc_position'] = $res->doc_position?$res->doc_position:"";
            // $res['received_date'] = $res->received_date?$res->received_date:"";
            // $res['submited_date'] = $res->submited_date?$res->submited_date:"";
            // $res['verification_time'] = "";
            // $res['status_color'] = $warna;
            $res['doc'] = "pr";
        
            return $res;
        });
        
        $data = array(
            'data'      => $result['data'],
        
        );
        return $result;
    }

    public function getPRByID($id){
        $result = DB::table("pr")
        ->select('pr_id as id','pr_num as num','description AS title','status','doc_status','doc_position','process_owner_id as owner_id','process_owner_nip as owner_nip','prid','received_date','submited_date','is_staf')
                ->where('pr_id',$id)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getPRByNum($num){
        $result = DB::table("pr")
        ->select('pr_id as id','pr_num as num','description AS title','status','doc_status','doc_position','received_date','submited_date','process_owner_id','process_owner_nip')
                ->where('pr_num',$num)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getLastTrackingStatus($id,$doc){
        $result = DB::table("tracking_status as ts")
                ->leftJoin('workflow as w','w.workflow_id','=','ts.workflow_id')
                ->select('ts.*','w.num_flow')
                ->where('source_id',$id)
                ->where('source_doc',$doc)
                ->orderBy('id','DESC')
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function checkWorkflowUser($nip,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('wu.user_nip',$nip)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getNextWorkflow($num_flow,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('w.num_flow',$num_flow)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function scanPR($id){
        $result = DB::table("pr")
                ->select('pr_id','pr_num','description AS title','ts.status','ts.workflow AS doc_position','ts.received_date','ts.submited_date',DB::raw('IF(ts.received_date IS NULL,"received","submit") action'))
                ->leftJoin('tracking_status as ts','ts.source_id','=','pr.pr_id')
                ->where('pr_id',$id)
                ->orderBY('id','DESC')
                ->first();
        /*$result = DB::select("
            SELECT ts.id,pr.pr_id,pr.pr_num,pr.description AS title,ts.status,ts.workflow doc_position,ts.received_date,ts.submited_date,IF(ts.received_date IS NULL,'received','submit') action
            FROM pr 
            LEFT JOIN tracking_status as ts ON ts.source_id=pr.pr_id
        
            WHERE pr_id=".$id."
            ORDER BY id DESC
            LIMIT 1
        ");*/
        return $result;
    }

    public function scanNumPR($num){
        $result = DB::table("pr")
                ->select('pr_id','pr_num','description AS title','pr.status','pr.doc_position','pr.routing_instruction as instruction','pr.memo','ts.submited_date',DB::raw('pr.received_date as received_date, IF(pr.received_date IS NULL,"received","submit") as action'))
                ->leftJoin('tracking_status as ts', function($join) {
                    $join->on('pr.pr_id', '=', 'ts.source_id');
                    $join->on('pr.doc', '=', 'ts.source_doc');
                })
                ->where('pr.pr_num',$num)
                ->orderBY('ts.id','ASC')
                ->first();
        return $result;
    }

    public function receivedPR($req){
        $pr = $this->getPRByID($req->id);
        $user = auth()->user();
        $last_status = $this->getLastTrackingStatus($req->id,"pr");
        if($last_status){

            $next_ = $last_status->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"pr");

            $data = array(
                'status'        => $pr->status,
                'received_date' => date('Y-m-d H:i:s'),
                'updated_by'    => $user->nip
            );
            if($last_status->user_nip=="" OR $last_status->workflow==""){
                $data['user_nip'] = $pr->owner_nip;
            }
            $query = DB::table('tracking_status')
                    ->where('id',$last_status->id)
                    ->update($data);
        }else{

            $check_wu = $this->checkWorkflowUser($user->nip,"pr");
            $next_ = $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"pr");

            $insert = array(
                'source_id'     => $req->id,
                'source_doc'    => "pr",
                'user_id'       => $user->user_id,
                'user_nip'      => $user->nip,
                //'status'        => $check_wu->status,
                'status'        => "On-Progress",
                'workflow'      => $check_wu->workflow_name,
                'workflow_id'   => $check_wu->workflow_id,
                'keterangan'    => $req->note,
                'sent_by'       => $user->nip,
                'sent_date'     => date('Y-m-d H:i:s'),
                'received_date' => date('Y-m-d H:i:s'),
                //'submited_date' => date('Y-m-d H:i:s'),
                'created_by'    => date('Y-m-d H:i:s'),
            );

            $query = DB::table('tracking_status')->insert($insert);

            
        }
        $data_pr = array(
            //'status'=>'PRINPG',
            //'doc_status'=>'PRINPG',
            'received_date'=>date('Y-m-d H:i:s')
        );

        $update = DB::table('pr')
                    ->where('pr_id',$req->id)
                    ->update($data_pr);

        if($query){
            $return = true;
        }else{
            $return = false;
        }
    }

    /*public function submitPR($req){
        $user = auth()->user();
        $verifikasi = $req->verifikasi;
        $pr = $this->getPRByID($req->id);
        if(strtolower($pr->status)=="closed"){
            return false;
        }
        if($pr){
            $last_status = $this->getLastTrackingStatus($req->id,"pr");
            // jika belum ada doc d tracking_status
            $check_wu = $this->checkWorkflowUser($user->nip,"pr");
            $next_ = $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"pr");

            if(intval($req->verifikasi)==1){
                // jika verifikasi ok 
                if($last_status){
                    $next_ = $last_status->num_flow+1;
                    $next_flow = $this->getNextWorkflow($next_,"pr");

                    if($last_status->num_flow==1){
                        $status_sub = "Send";
                    }else{
                        $status_sub = "Approved";
                    }

                    $data = array(
                        'status'        => $status_sub,
                        'keterangan'    => $req->note,
                        'submited_date' => date('Y-m-d H:i:s'),
                        'updated_by'    => $user->nip
                    );
                    //update submited
                    $update = DB::table('tracking_status')
                            ->where('id',$last_status->id)
                            ->update($data);

                    if($last_status->num_flow==1 AND $last_status->revisi==1){
                        $status = "RESEND";
                    }else{
                        $status = "Waiting Approval";
                    }
                    $workflow_id = $next_flow->workflow_id;
                    // insert for next status  
                    
                    $insert_next = array(
                        'source_id'     => $req->id,
                        'source_doc'    => "pr",
                        'source_num'    => $req->num,
                        'user_id'       => $next_flow->user_id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => "Waiting Approval",
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status')->insert($insert_next);
                    if($query){
                        $return = true;
                        $position = $next_flow->workflow_name;
                        $user_nip = $next_flow->user_nip;
                        
                    }else{
                        $return = false;
                    }
                    //return $last_status;
                }else{
                    $status = "Waiting Approval";
                    //insert first to tracking status
                    $insert = array(
                        'source_id'     => $req->id,
                        'source_doc'    => "pr",
                        'source_num'    => $req->num,
                        'user_id'       => $user->user_id,
                        'user_nip'      => $user->nip,
                        'status'        => $check_wu->status,
                        'workflow'      => $check_wu->workflow_name,
                        'workflow_id'   => $check_wu->workflow_id,
                        'keterangan'    => $req->note,
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        'received_date' => date('Y-m-d H:i:s'),
                        'submited_date' => date('Y-m-d H:i:s'),
                        'created_by'    => date('Y-m-d H:i:s'),
                    );

                    DB::table('tracking_status')->insert($insert);

                    // insert for next status
                    $workflow_id = $next_flow->workflow_id;
                    $insert_next = array(
                        'source_id'     => $req->id,
                        'source_doc'    => "pr",
                        'source_num'    => $req->num,
                        'user_id'       => $next_flow->user_id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => $status,
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status')->insert($insert_next);
                    
                    if($query){
                        $return = true;
                        $position = $next_flow->workflow_name;
                        $user_nip = $next_flow->user_nip;
                    }else{
                        $return = false;
                    }
                }
            }else{
                // jika di tolak/revisi
                if($last_status->num_flow>5){
                    $revisi = "PLNREVISE";
                    $next_ = 5;
                    $next_flow = $this->getNextWorkflow($next_,"pr");
                }
                else if($last_status->num_flow==5){
                    $revisi = "PLNREVISE";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"pr");
                }
                else{
                    $revisi = "MODIFY";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"pr");
                }
                if($req->verifikasi=='0'){
                    $status = "Revise";
                }else{
                    $status = $revisi;
                }
                
                $data = array(
                    'status'        => $status,
                    'keterangan'    => $req->note,
                    'submited_date' => date('Y-m-d H:i:s'),
                    'updated_by'    => $user->nip,
                    'revisi'        => 1
                );
                //update submited
                $update = DB::table('tracking_status')
                        ->where('id',$last_status->id)
                        ->update($data);

                $workflow_id = $next_flow->workflow_id;

                $insert_next = array(
                    'source_id'     => $req->id,
                    'source_doc'    => "pr",
                    'source_num'    => $req->num,
                    'user_id'       => $next_flow->user_id,
                    'user_nip'      => $next_flow->user_nip,
                    'status'        => $status,
                    'workflow'      => $next_flow->workflow_name,
                    'workflow_id'   => $next_flow->workflow_id,
                    'keterangan'    => $req->note,
                    'sent_by'       => $user->nip,
                    'sent_date'     => date('Y-m-d H:i:s'),
                    //'received_date' => "",
                    //'submited_date' => "",
                    'created_by'    => date('Y-m-d H:i:s'),
                    'revisi'        => 1
                );
                $query = DB::table('tracking_status')->insert($insert_next);
                if($query){
                    $return = true;
                    $position = $next_flow->workflow_name;
                    $user_nip = $next_flow->user_nip;
                }else{
                    $return = false;
                }

            }
            
        }else{
            $return = false;
        }
        $data_pr = array(
            'status'=>$status,
            'doc_status'=>$status,
            'doc_position' => $position,
            'process_owner_nip' => $user_nip,
            'sent_date' => date('Y-m-d H:i:s'),
            'received_date' => null
        );

        $update = DB::table('pr')
                    ->where('pr_id',$req->id)
                    ->update($data_pr);
        if($update){

            $title="Dokumen Masuk PR NO : ".$pr->num;
            $pesan ="PR NO. ".$pr->num." telah dikirim ke meja Bapak/Ibu, harap untuk segera diterima.";
            $this->sendPushNotification($user->token_fcm,$title,$pesan);
            $workflow = $this->getWorkFlowUserID($workflow_id);
            foreach ($workflow as $key) {
                $data_msg = array(
                    'sent_by'       => $user->nip,
                    'receipt_by'    => $key->user_nip,
                    'title'         => $title,
                    'message'       => $pesan,
                    'created_by'    => $user->user_id,
                    'created_date'  => date('Y-m-d H:i:s')
                );
                $this->sendMessage($data_msg);
            }
            
        }
        return $return;
    }*/

    public function closePR($req){
        $id = $req->id;
        $user = auth()->user();
        $last_status = $this->getLastTrackingStatus($req->id,"pr");
        $data = array(
            'status'        => "Closed",
            'keterangan'    => "close PR",
            'submited_date' => date('Y-m-d H:i:s'),
            'updated_by'    => $user->nip
        );
        //update submited
        $update = DB::table('tracking_status')
                ->where('id',$last_status->id)
                ->update($data);

        $data_pr = array(
            'status'        => "Closed",
            'doc_status'    => "Closed",
            'submited_date' => date('Y-m-d H:i:s')
        );

        $update_pr = DB::table('pr')
                    ->where('pr_id',$req->id)
                    ->update($data_pr);

        return $update_pr;
    }

    public function createPRMaximo($req){
        $pr = $this->getPRByNum($req->pr_num);
        $data_pr = array(
            'pr_num'                    => $req->pr_num,
            'budget_categories'         => $req->budget_categories,
            'asset'                     => $req->asset,
            'description'               => $req->description,
            'priority'                  => $req->priority,
            'total_cost'                => $req->total_cost,
            'status'                    => @$req->status?$req->status:"Created",
            'status_description'        => @$req->status_description?$req->status_description:"",
            'doc_status'                => @$req->status?$req->status:"Created",
            'doc_position'              => @$req->position?$req->position:"Officer",
            'remarks'                   => @$req->remarks?$req->remarks:"",
            'routing_instruction'       => @$req->routing_instruction?$req->routing_instruction:"",
            'prid'                      => @$req->prid?$req->prid:"",
            'process_owner_id'          => @$req->ownerid,
            'process_owner_nip'         => @$req->ownernip,
            'required_date'             => $req->required_date,
        );

        if($pr){
            if(strtolower($req->status)=='concur'){
                $data_pr['ae_rendal'] = date('Y-m-d H:i:s');
            }
            $data_pr['received_date']         = NULL;
            $query = DB::table('pr')->where('pr_num',$req->pr_num)->update($data_pr);
            $last_status = $this->getLastTrackingStatus($pr->id,"pr");
            if($last_status->user_nip!=@$req->ownernip){
                
                $data = array(
                    'submited_date' => date('Y-m-d H:i:s'),
                    'updated_by'    => @$req->ownernip
                );
                if($last_status->user_nip=="" OR $last_status->workflow==""){
                    $data['user_nip'] = @$pr->process_owner_nip;
                }
                //update submited
                $update = DB::table('tracking_status')
                        ->where('id',$last_status->id)
                        ->update($data);
                
                $insert_ = array(
                    'source_id'     => $pr->id,
                    'source_num'    => $req->pr_num,
                    'source_doc'    => "pr",
                    'user_nip'      => @$req->ownernip,
                    'status'        => $req->status,
                    'workflow'      => $req->position,
                    'workflow_id'   => "",
                    'keterangan'    => @$req->remarks,
                    'sent_by'       => @$pr->process_owner_nip,
                    'sent_date'     => date('Y-m-d H:i:s'),
                    'created_by'    => date('Y-m-d H:i:s'),
                );
                if(@$req->ownernip!=""){
                    $query = DB::table('tracking_status')->insert($insert_);
                }
                
            }
        }else{
            $data_pr['created_by'] = $req->created_by;
            $data_pr['date_created'] = date('Y-m-d H:i:s');
            $data_pr['sent_date'] = date('Y-m-d H:i:s');
            $query = DB::table('pr')->insert($data_pr);
            $pr_id = DB::getPdo()->lastInsertId();
            $insert_ = array(
                'source_id'     => $pr_id,
                'source_num'    => $req->pr_num,
                'source_doc'    => "pr",
                'user_nip'      => @$req->ownernip,
                'status'        => @$req->status,
                'workflow'      => @$req->position,
                'workflow_id'   => "",
                'keterangan'    => @$req->remarks,
                'sent_by'       => "",
                'sent_date'     => date('Y-m-d H:i:s'),
                'received_date' => date('Y-m-d H:i:s'),
                'created_by'    => date('Y-m-d H:i:s'),
            );
            $query = DB::table('tracking_status')->insert($insert_);
        }
        

        if($query){
            return true;
        }else{
            return false;
        }
         
    }

    public function updatePRMaximo($req){
        
        $data_pr = array(
            'pr_num'                    => $req->pr_num,
            'budget_categories'         => $req->budget_categories,
            'asset'                     => $req->asset,
            'description'               => $req->description,
            'priority'                  => $req->priority,
            'total_cost'                => $req->total_cost,
            'status'                    => @$req->status?$req->status:"Created",
            'status_description'        => @$req->status_description?$req->status_description:"",
            'doc_status'                => @$req->status?$req->status:"Created",
            'doc_position'              => @$req->position?$req->position:"Officer",
            'remarks'                   => @$req->remarks?$req->remarks:"",
            'routing_instruction'       => @$req->routing_instruction?$req->routing_instruction:"",
            'process_owner_id'          => @$req->ownerid,
            'process_owner_nip'         => @$req->ownernip,
            'required_date'             => $req->required_date,
            'modify_by'                 => $req->updated_by,
            'date_updated'              => date('Y-m-d H:i:s')
        );
        $query = DB::table('pr')->where('pr_num',$req->pr_num)->update($data_pr);

        if($query){
            return true;
        }else{
            return false;
        }
         
    }

    public function RoutingPRMaximo($req){
        $pr = $this->getPRByNum($req->pr_num);

        $data_pr = array(
            'status'                => $req->status,
            'doc_status'            => $req->status,
            'status_description'    => $req->status_description,
            'doc_position'          => $req->position,
            'process_owner_id'      => $req->ownerid,
            'process_owner_nip'     => $req->ownernip,
            //'remarks'               => @$req->note,
            'routing_instruction'   => $req->routing_instruction,
            'sent_date'             => date('Y-m-d H:i:s'),
            'received_date'         => NULL
            //'submited_date'         => date('Y-m-d H:i:s')
        );
        if($pr){
            if(strtolower($req->status)=='concur'){
                $data_pr['ae_rendal'] = date('Y-m-d H:i:s');
            }else if(strtolower($req->status)=='concur1'){
                $data_pr['msb_ophar'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='concur1.1'){
                $data_pr['amn_har'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='concur2'){
                $data_pr['msb_ophar2'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='concur3'){
                $data_pr['msb_log'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='concur4'){
                $data_pr['srm_produksi'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='srmreview' OR strtolower($req->status)=='srmreviewed'){
                $data_pr['gm'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='srmappr' OR strtolower($req->status)=='gmmappr'){
                $data_pr['ae_rendal2'] = date('Y-m-d H:i:s');
            }
            $submit_pr = DB::table('pr')
                        ->where('pr_num',$req->pr_num)
                        ->update($data_pr);
            $pr_id  = $pr->id;
            
        }else{
            $data_pr['pr_num'] = $req->pr_num;
            $data_pr['description'] = @$req->description;
            $data_pr['date_created'] = date('Y-m-d H:i:s');

            $submit_pr = DB::table('pr')->insert($data_pr);
            $pr_id = DB::getPdo()->lastInsertId();
        }

        if($submit_pr){            

            $last_status = $this->getLastTrackingStatus($pr_id,"pr");
            if($last_status){
                
                $data = array(
                    //'status'        => $req->status,
                    //'user_nip'      => @$req->ownernip,
                    //'keterangan'    => @$req->remarks,
                    'submited_date' => date('Y-m-d H:i:s'),
                    'updated_by'    => @$req->ownernip
                );
                if($last_status->user_nip=="" OR $last_status->workflow==""){
                    $data['user_nip'] = @$pr->process_owner_nip;
                }
                //update submited
                $update = DB::table('tracking_status')
                        ->where('id',$last_status->id)
                        ->update($data);
            }else{
                
                $insert_ = array(
                    'source_id'     => $pr->id,
                    'source_num'    => $req->pr_num,
                    'source_doc'    => "pr",
                    'user_nip'      => @$req->ownernip,
                    'status'        => $req->status,
                    'workflow'      => $req->position,
                    'workflow_id'   => "",
                    'keterangan'    => @$req->remarks,
                    'sent_by'       => @$pr->process_owner_nip,
                    'sent_date'     => date('Y-m-d H:i:s'),
                    'received_date' => date('Y-m-d H:i:s'),
                    'submited_date' => date('Y-m-d H:i:s'),
                    'created_by'    => date('Y-m-d H:i:s'),
                );
                $query = DB::table('tracking_status')->insert($insert_);
            }
            
            $insert_next = array(
                'source_id'     => $pr->id,
                'source_num'    => $req->pr_num,
                'source_doc'    => "pr",
                'user_nip'      => @$req->ownernip,
                //'status'      => "Waiting Received",
                'status'        => $req->status,
                'workflow'      => @$req->position,
                'workflow_id'   => "",
                //'keterangan'    => "",
                'sent_by'       => @$pr->process_owner_nip,
                'sent_date'     => date('Y-m-d H:i:s'),
                //'received_date' => "",
                //'submited_date' => "",
                'created_by'    => date('Y-m-d H:i:s'),
            );
            $query = DB::table('tracking_status')->insert($insert_next);

            $user = DB::table('sys_users')->where('nip',@$req->ownernip)->first();
            $this->createLogResponseMaximo(@$req->ownernip,"get user submit RR",json_encode($user));
            if($user){
                $title="Masuk PR NO : ".$pr->num;
                $pesan ="PR NO. ".$pr->num." telah dikirim, harap untuk segera diterima.";
                $this->sendPushNotification($user->token_fcm,$title,$pesan);
                $data_msg = array(
                    'sent_by'       => @$pr->process_owner_nip,
                    'receipt_by'    => @$user->nip,
                    'title'         => $title,
                    'message'       => $pesan,
                    'created_by'    => @$user->user_id,
                    'created_date'  => date('Y-m-d H:i:s')
                );
                $this->sendMessage($data_msg);
                $this->createLogResponseMaximo(@$req->ownernip,"send inbox",json_encode($user));
            }
            
        }
        
    }

    public function submitFromTjbsc($req,$token=""){
        //array
        $data = array(
            "memo"=>"memo terbaru",
            "accepted"=>true
        );

        $dataObj = '{
            "memo":"memo terbaru",
            "accepted":true
        }';

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://10.47.0.33/maximo/oslc/os/mxapiwfassignment/5346?action=wsmethod:completeAssignment',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            'MAXAUTH: ODUxMDIxOVo6ODUxMDIxOVo=',
            'x-method-override: PATCH',
            'patchtype: MERGE',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

    }

    public function submitPRMobile($req,$token=""){
        

        

    }


    

}

?>