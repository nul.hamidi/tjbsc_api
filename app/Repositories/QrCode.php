<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\TrackingStatus;
use App\Repositories\Api;
use App\Repositories\PurRequest;
use App\Repositories\PurOrder;
use App\Repositories\GoodsReceipt;
use App\Repositories\InvoiceRepo;

class QrCode extends Api{

    public function scanPR($num) {
        $user = auth()->user();
        $purchaceRequest = new PurRequest();
        /*$result = DB::table("pr")
        ->select('pr_id','pr_num','description AS title','status','doc_status','doc_position','received_date','submited_date')
                ->where('pr_id',$id)
                ->first();
        if($result){
            return $return = array(
                'id' => $result->pr_id,
                'num' => $result->pr_num,
                'title' => $result->title,
                'status' => $result->status,
                'doc_status' => $result->doc_status,
                'doc_position' => $result->doc_position,
                'doc' =>'pr',
                'action' => 'received' 
            );
        }*/
        
        $result = $purchaceRequest->scanNumPR($num);
        $last_ = $purchaceRequest->getLastTrackingStatus($result->pr_id,"pr");
        if($result->action=="submit" AND $last_->num_flow>13){
            $action = "close";
        }else{
            $action = $result->action;
        }
        if($result){

            $staffs = $this->getUserDisposisi();

            //if($user->nip=='8002021A2'){
            if($result->status=='CONCUR1'){
                $staf = $staffs;
            }else{
                $staf =  false;
            }

            return $return = array(
                'id' => $result->pr_id,
                'num' => $result->pr_num,
                'title' => $result->title,
                'status' => $result->status,
                'doc_status' => $result->status,
                'doc_position' => $result->doc_position,
                'instruction' => $result->instruction?$result->instruction:"Please review this PR",
                'received_date' => $result->received_date?date('H:i, d M Y',strtotime($result->received_date)):'-',
                'memo'          => $result->memo?$result->memo:'-', 
                'doc' =>'pr',
                'action' => $result->action,
                'staff' => $staf
            );
        }
        return false;
    }

    public function scanPO($num) {
        $purchaceOrder = new PurOrder();
        $result = $purchaceOrder->scanNumPO($num);
        $last_ = $purchaceOrder->getLastTrackingStatus($result->po_id,"po");
        if($result->action=="submit" AND $last_->num_flow>9){
            $action = "close";
        }else{
            $action = $result->action;
        }
        if($result){
            return $return = array(
                'id' => $result->po_id,
                'num' => $result->po_num,
                'title' => $result->title,
                'status' => $result->status,
                'doc_status' => $result->status,
                'doc_position' => $result->doc_position,
                'instruction' => $result->instruction?$result->instruction:"Please review this PO",
                'received_date' => $result->received_date?date('H:i, d M Y',strtotime($result->received_date)):'-',
                'memo'          => $result->memo?$result->memo:'-', 
                'doc' =>'po',
                'action' => $result->action,
                'staff' => false
            );
        }
        
        return false;
    }

    public function scanGR($num) {
        $GoodReceipt = new GoodsReceipt();
        $result = $GoodReceipt->scanNumGR($num);
        if($result){
            return $return = array(
                'id' => $result->id,
                'num' => $result->num,
                'title' => $result->title,
                'status' => $result->status,
                'doc_position' => $result->doc_position,
                'instruction' => $result->instruction?$result->instruction:"Please review this Goods Receipt",
                'received_date' => $result->received_date?date('H:i, d M Y',strtotime($result->received_date)):'-',
                'memo'          => $result->memo?$result->memo:'-', 
                'doc' =>'gr',
                'action' => $result->action,
                'staff' => false
            );
        }
        
        return false;
    }

    public function scanInvoice($num) {
        $invoice = new InvoiceRepo();
        $result = $invoice->scanNumInvoice($num);
        if($result){
            return $return = array(
                'id' => $result->id,
                'num' => $result->num,
                'title' => $result->title,
                'status' => $result->status,
                'doc_position' => $result->doc_position,
                'instruction' => $result->instruction?$result->instruction:"Please review this Invoice",
                'received_date' => $result->received_date?date('H:i, d M Y',strtotime($result->received_date)):'-',
                'memo'          => $result->memo?$result->memo:'-', 
                'doc' =>'invoice',
                'action' => $result->action,
                'staff' => false
            );
        }
        
        return false;
    }

    public function receivedAct($req){
        $id = $req->id;
        //$num = $req->num;
        $doc = $req->doc;
        $purchaceRequest = new PurRequest();
        $PurOrder = new PurOrder();
        $GoodReceipt = new GoodsReceipt();
        $invoice = new InvoiceRepo();
        if($doc=='pr'){
            $receipt = $purchaceRequest->receivedPR($req);
        }else if($doc=='po'){
            $receipt = $PurOrder->receivedPO($req);
        }else if($doc=='gr'){
            $receipt = $GoodReceipt->receivedGR($req);
        }else if($doc=='invoice'){
            $receipt = $invoice->receivedInvoice($req);
        }
        else{
            $receipt = false;
        }

        $return = array(
            'id'    => $req->id,
            'doc'   => $req->doc,
            'received'  => date('Y-m-d H:i')
        );
        return $return;
    }

    public function submited($req,$user){

        $user = auth()->user();

        $id = $req->id;
        $doc = $req->doc;
        if($user->site=='PLNTJB' OR $user->site=='PLN'){
            $IPAddress = ENV('IP_PLN');
        }else{
            $IPAddress = ENV('IP_TJBPS');
        }
        
        if(intval($req->verifikasi)==1){
            $accepted = true;
        }else{
            $accepted = false;
        }

        $data = array(
            "memo"      => $req->note,
            "accepted"  => $accepted
        );

        $purchaceRequest = new PurRequest();
        $PurOrder = new PurOrder();
        $GoodReceipt = new GoodsReceipt();
        $invoice = new InvoiceRepo();
        if($doc=='pr'){
            //$submit = $purchaceRequest->submitPR($req);
            $pr = $purchaceRequest->getPRByID($req->id);
            /*if($req->staf_id){
                if($pr->is_staf==0){
                    $this->apiSubmitUserDisposisi($pr->prid,$req->staf_id);
                    DB::table('pr')->where('pr_id',$req->id)->update(['is_staf'=>1]);
                    sleep(3);
                }else{
                    DB::table('pr')->where('pr_id',$req->id)->update(['is_staf'=>0]);
                }
            }*/
            //response()->json($pr)->send();die();
            $submit = $this->apiSubmitPR($IPAddress,$pr->owner_id, $data);
            DB::table('pr')->where('pr_id',$req->id)->update(['memo'=>$req->note]);
            //response()->json($submit)->send();die();
        }else if($doc=='po'){
            //$submit = $PurOrder->submitPO($req);
            $po = $PurOrder->getPOByID($req->id);
            $submit = $this->apiSubmitPO($IPAddress,$po->owner_id, $data);
            DB::table('po')->where('po_id',$req->id)->update(['memo'=>$req->note]);
        }else if($doc=='gr'){
            $submit = $GoodReceipt->submitGR($req);
        }else if($doc=='invoice'){
            $submit = $invoice->submitInvoice($req);
        }
        else{
            $submit = false;
        }
        if($submit){
            if(intval($req->verifikasi)==1){$status="Approved";}
            else{$status="Revise";}
            
            return $return = array(
                'id'            => $req->id,
                'num'           => $req->num,
                'doc'           => $req->doc,
                'verifikasi'    => @$req->verifikasi,
                'note'          => @$req->note,
                'submited'      => date('Y-m-d H:i'),
                'status'        => $status,
                'staff'         => false
            );
        }
        
        return false;
    }

    public function closed($req){
        $id = $req->id;
        $doc = $req->doc;
        $purchaceRequest = new PurRequest();
        $PurOrder = new PurOrder();
        $GoodReceipt = new GoodsReceipt();
        $invoice = new InvoiceRepo();
        if($doc=='pr'){
            $close = $purchaceRequest->closePR($req);
        }else if($doc=='po'){
            $close = $PurOrder->closePO($req);
        }else if($doc=='gr'){
            $close = $GoodReceipt->closeGR($id);
        }else if($doc=='invoice'){
            $close = $invoice->closeInvoice($id);
        }
        else{
            $close = false;
        }
        if($close){
            return $return = array(
                'id'            => $req->id,
                'doc'           => $req->doc,
                'closed_date'   => date('Y-m-d H:i'),
                'status'        => "closed"
            );
        }
        
        return $close;
    }

    public function submitStaf($req,$user){
        $purchaceRequest = new PurRequest();
        $pr = $purchaceRequest->getPRByID($req->id);
        if($req->staf_id){
                $this->apiSubmitUserDisposisi($pr->prid,$req->staf_id);
        }
        return true; 
    }



 


    

}

?>