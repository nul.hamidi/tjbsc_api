<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\InvoiceModel;
use App\Repositories\Api;

class InvoiceRepo extends Api{
    
    public function getInvoice($page,$req) {
        $return = array();
        $limit = @$req->limit?@$req->limit:20;
        $search = @$req->search?@$req->search:"";
        $doc = 'pr';
        $result = InvoiceModel::
                select('invoice.id as id','invoice.num','invoice.text AS title','invoice.routing_instruction as instruction','invoice.send_date AS sent_date','invoice.received_date','invoice.submited_date','invoice.created_date',DB::raw('IFNULL(invoice.status,st.status) status,IFNULL(invoice.position,st.workflow) doc_position,DATEDIFF(now(),IFNULL(invoice.send_date,invoice.created_date)) as days'))
                ->leftJoin(DB::raw('(SELECT invoice_id,status, workflow,received_date, submited_date from `tracking_status_invoice` group by id  order by id ASC) AS st'),'st.invoice_id','=','invoice.id')
                ->when($search, function ($query, $search) {
                     return $query->where('num','like','%'.$search.'%');
                })
                ->when($search, function ($query, $search) {
                    return $query->orWhere('text','like','%'.$search.'%');
                })
                ->groupBy('invoice.id')
                ->paginate($limit,$page);
            //->toSql();
        $result->map(function($res){
            if($res->id=='664737016'){
                $warna = "#dc143c";//merah
            }else if($res->id=='664738723'){
                $warna = "#f0f000";//kuning
            }
            else{
                
                $warna = "#0089C7";//biru
            }
            $warna = "#0089C7";//biru
            $res['status'] = $res->status?$res->status:"";
            $res['doc_position'] = $res->doc_position?$res->doc_position:"";
            $res['instruction'] = $res->instruction?$res->instruction:"Please review this invoice";
            $res['sent_date'] = $res->sent_date?date('d M Y',strtotime($res->sent_date)):"-";
            $res['received_date'] = $res->received_date?$res->received_date:"";
            $res['submited_date'] = $res->submited_date?$res->submited_date:"";
            $res['created_date'] = $res->created_date?date('d M Y',strtotime($res->created_date)):"";
            $res['verification_time'] = $res->days?$res->days:"-";
            $res['status_color'] = $warna;
            $res['doc'] = "invoice";
        
            return $res;
        });
        
        $data = array(
            'data'      => $result['data'],
            //'last_page' => $result->last_page,
            //'total'     => $result->total
        );
        return $result;
    }

    public function getInvoiceByID($id){
        $result = DB::table("invoice")
        ->select('id','num','text AS title','status','position','routing_instruction as instruction','memo','send_date as sent_date','received_date','submited_date','created_date')
                ->where('id',$id)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getInvoiceByNUM($num){
        $result = DB::table("invoice")
        ->select('id','num','text AS title','status','position','received_date','submited_date')
                ->where('num',$num)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getLastTrackingStatus($id,$doc){
        $result = DB::table("tracking_status_invoice as ts")
                ->leftJoin('workflow as w','w.workflow_id','=','ts.workflow_id')
                ->select('ts.*','w.num_flow')
                ->where('ts.invoice_id',$id)
                ->orderBy('ts.id','DESC')
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function checkWorkflowUser($nip,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('wu.user_nip',$nip)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getNextWorkflow($num_flow,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('w.num_flow',$num_flow)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function scanInvoice($id){
        $result = DB::table("invoice")
                ->select('invoice.id','invoice.num','invoice.text AS title','ts.status','ts.workflow AS doc_position','ts.received_date','ts.submited_date',DB::raw('IF(ts.received_date IS NULL,"received","submit") action'))
                ->leftJoin('tracking_status_invoice as ts','ts.invoice_id','=','invoice.id')
                ->where('invoice.id',$id)
                ->orderBY('ts.id','DESC')
                ->first();
        
        return $result;
    }

    public function scanNumInvoice($num){
        $result = DB::table("invoice")
                ->select('invoice.id','invoice.num','invoice.text AS title','invoice.status','ts.workflow AS doc_position','invoice.routing_instruction as instruction','invoice.memo','ts.received_date','ts.submited_date',DB::raw('IF(ts.received_date IS NULL,"received","submit") action'))
                ->leftJoin('tracking_status_invoice as ts','ts.invoice_id','=','invoice.id')
                ->where('invoice.num',$num)
                ->orderBY('ts.id','DESC')
                ->first();
        
        return $result;
    }

    public function receivedInvoice($req){
        $user = auth()->user();
        $last_status = $this->getLastTrackingStatus($req->id,"invoice");
        if($last_status){

            $next_ = $last_status->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"invoice");

            $data = array(
                'status'        => 'On-Progres',
                'received_date' => date('Y-m-d H:i:s'),
                'updated_by'    => $user->nip
            );
            $query = DB::table('tracking_status')
                    ->where('id',$last_status->id)
                    ->update($data);
        }else{

            $check_wu = $this->checkWorkflowUser($user->nip,"invoice");
            $next_ = $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"invoice");

            $insert = array(
                'invoice_id'     => $req->id,
                'user_nip'      => $user->nip,
                //'status'        => $check_wu->status,
                'status'        => "On-Progres",
                'workflow'      => $check_wu->workflow_name,
                'workflow_id'   => $check_wu->workflow_id,
                'keterangan'    => $req->note,
                'sent_by'       => $user->nip,
                'sent_date'     => date('Y-m-d H:i:s'),
                'received_date' => date('Y-m-d H:i:s'),
                //'submited_date' => date('Y-m-d H:i:s'),
                'created_by'    => date('Y-m-d H:i:s'),
            );

            $query = DB::table('tracking_status_invoice')->insert($insert);

            
        }
        $data_invoice = array(
            'status'=>'On-Progres',
            'received_date'=>date('Y-m-d H:i:s')
        );

        $update = DB::table('invoice')
                    ->where('id',$req->id)
                    ->update($data_invoice);

        if($query){
            $return = true;
        }else{
            $return = false;
        }
    }

    public function submitInvoice($req){
        $verifikasi = $req->verifikasi;
        $invoice = $this->getInvoiceByID($req->id);
        $user = auth()->user();
        if($invoice){
            $last_status = $this->getLastTrackingStatus($req->id,"invoice");
            // jika belum ada doc d tracking_status
            $check_wu = $this->checkWorkflowUser($user->nip,"invoice");
            $next_ = $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"invoice");

            if($req->verifikasi==1 OR $req->verifikasi==3){
                // jika verifikasi ok 
                if($last_status){
                    $next_ = $last_status->num_flow+1;
                    $next_flow = $this->getNextWorkflow($next_,"invoice");

                    if($last_status->num_flow==1){
                        $status_sub = "Send";
                    }else{
                        $status_sub = "Approved";
                    }

                    $data = array(
                        'status'        => $status_sub,
                        'keterangan'    => $req->note,
                        'submited_date' => date('Y-m-d H:i:s'),
                        'updated_by'    => $user->nip
                    );
                    //update submited
                    $update = DB::table('tracking_status_invoice')
                            ->where('id',$last_status->id)
                            ->update($data);

                    if($last_status->num_flow==1 AND $last_status->revisi==1){
                        $status = "RESEND";
                    }else{
                        $status = "Waiting Approval";
                    }
                    // insert for next status        
                    $insert_next = array(
                        'invoice_id'     => $req->id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => "Waiting Approval",
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status_invoice')->insert($insert_next);
                    if($query){
                        $return = true;
                    }else{
                        $return = false;
                    }
                    //return $last_status;
                }else{
                    $status = "Waiting Approval";
                    //insert first to tracking status
                    $insert = array(
                        'invoice_id'     => $req->id,
                        'user_nip'      => $user->nip,
                        'status'        => $check_wu->status,
                        'workflow'      => $check_wu->workflow_name,
                        'workflow_id'   => $check_wu->workflow_id,
                        'keterangan'    => $req->note,
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        'received_date' => date('Y-m-d H:i:s'),
                        'submited_date' => date('Y-m-d H:i:s'),
                        'created_by'    => date('Y-m-d H:i:s'),
                    );

                    DB::table('tracking_status_invoice')->insert($insert);

                    // insert for next status
                    $insert_next = array(
                        'invoice_id'     => $req->id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => $status,
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status_invoice')->insert($insert_next);
                    if($query){
                        $return = true;
                    }else{
                        $return = false;
                    }
                }
            }else{
                // jika di tolak/revisi
                if($last_status->num_flow>5){
                    $revisi = "PLNREVISE";
                    $next_ = 5;
                    $next_flow = $this->getNextWorkflow($next_,"invoice");
                }
                else if($last_status->num_flow==5){
                    $revisi = "PLNREVISE";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"invoice");
                }
                else{
                    $revisi = "MODIFY";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"invoice");
                }
                if($req->verifikasi=='1'){
                    $status = "Reject";
                }else{
                    $status = $revisi;
                }
                
                $data = array(
                    'status'        => $status,
                    'keterangan'    => $req->note,
                    'submited_date' => date('Y-m-d H:i:s'),
                    'updated_by'    => $user->nip,
                    'revisi'        => 1
                );
                //update submited
                $update = DB::table('tracking_status_invoice')
                        ->where('id',$last_status->id)
                        ->update($data);

                $insert_next = array(
                    'invoice_id'     => $req->id,
                    'user_nip'      => $next_flow->user_nip,
                    'status'        => $status,
                    'workflow'      => $next_flow->workflow_name,
                    'workflow_id'   => $next_flow->workflow_id,
                    'keterangan'    => $req->note,
                    'sent_by'       => $user->nip,
                    'sent_date'     => date('Y-m-d H:i:s'),
                    //'received_date' => "",
                    //'submited_date' => "",
                    'created_by'    => date('Y-m-d H:i:s'),
                    'revisi'        => 1
                );
                $query = DB::table('tracking_status_invoice')->insert($insert_next);
                if($query){
                    $return = true;
                }else{
                    $return = false;
                }

            }
            
        }else{
            $return = false;
        }
        $data_invoice = array(
            'status'=>$status,
            'submited_date'=>date('Y-m-d H:i:s')
        );

        $update = DB::table('invoice')
                    ->where('id',$req->id)
                    ->update($data_invoice);
        return $return;
    }


    

}

?>