<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\PurchaseOrder;
use App\Repositories\Api;

class PurOrder extends Api{
    
    public function getPOrder($page,$req) {
        $return = array();
        $limit = @$req->limit?@$req->limit:20;
        $search = @$req->search?@$req->search:"";
       
        $result = PurchaseOrder::
                select('po.po_id as id','po.po_num as num','po.description AS title','po.routing_instruction as instruction','po.sent_date','st.received_date','st.submited_date','po.created_date','po.process_owner_id AS ownerid',DB::raw('IFNULL(u.name,po.doc_position) position,IFNULL(process_owner_nip,"-") as ownernip, IFNULL(po.status,st.status) status,IFNULL(po.doc_status,st.status) doc_status,IFNULL(po.doc_position,st.workflow) doc_position,DATEDIFF(now(),IFNULL(po.sent_date,po.created_date)) as days'))
                //->leftJoin(DB::raw('(SELECT source_id,source_doc, status, workflow,received_date, submited_date from `tracking_status` group by id order by id DESC) AS st'),'st.source_id','=','po.po_id')
                ->leftJoin(DB::raw('(SELECT source_id,source_doc, status, workflow,received_date, submited_date from `tracking_status` group by id order by id ASC) AS st'), function($join) {
                    $join->on('po.po_id', '=', 'st.source_id');
                    $join->on('po.doc', '=', 'st.source_doc');
                })
                ->leftJoin('sys_users as u','u.nip','=','po.process_owner_nip')
                ->when($search, function ($query, $search) {
                    return $query->where('po.po_num','like','%'.$search.'%');
                })
                ->when($search, function ($query, $search) {
                    return $query->orWhere('po.description','like','%'.$search.'%');
                })
                ->groupBy('po.po_id')
                ->orderBy('po_id','DESC')
                ->paginate($limit,$page);

        $result->map(function($res){

            if($res->id=='664733521'){
                $warna = "#dc143c";//merah
            }else if($res->id=='664738723'){
                $warna = "#664741870";//kuning
            }
            else{
                $warna = "#0089C7";//biru
            }

            if($res->received_date!=""){
                $warna = "#3cb371";//hijau
            }else{
                $warna = "#0089C7";//biru
            }

            
            $res['doc_status'] = $res->doc_status?$res->doc_status:"";
            $res['doc_position'] = $res->position?$res->position:$res->ownernip;
            $res['instruction'] = $res->instruction?$res->instruction:"Please review this PO";
            $res['sent_date'] = $res->sent_date?date('d M Y',strtotime($res->sent_date)):"-";
            $res['received_date'] = $res->received_date?$res->received_date:"";
            $res['submited_date'] = $res->submited_date?$res->submited_date:"";
            $res['created_date'] = $res->created_date?date('d M Y',strtotime($res->created_date)):"";
            $res['verification_time'] = $res->days?$res->days:"-";
            $res['status_color'] = $warna;
            $res['doc'] = "po";
        
            return $res;
        });
        /*foreach ($result as $key) {
            $return[] = array(
                'id' => $key->id,
                'num' => $key->num,
                'title' => $key->title,
                'status' => $key->status,
                'doc_status' => $key->doc_status,
                'doc_position' => $key->doc_position,
                'received_date' => $key->received_date,
                'submited_date' => $key->submited_date,
                'lama_verifikasi' => "",
                'doc' =>'po',
            );
        }*/
        return $result;
    }

    public function getPOrderMaximo($page,$req) {
        $return = array();
        $limit = @$req->limit?@$req->limit:20;
        $search = @$req->search?@$req->search:"";
       
        $result = PurchaseOrder::
                select('po_id as id','po_num','budget_categories','description','status','status_description','doc_position as position','total_cost','remarks','routing_instruction','ordered_date','created_by','process_owner_id AS ownerid','process_owner_nip as ownernip','created_date')
               
                ->when($search, function ($query, $search) {
                    return $query->where('po_num','like','%'.$search.'%');
                })
                ->when($search, function ($query, $search) {
                    return $query->orWhere('description','like','%'.$search.'%');
                })
                ->groupBy('po_id')
                ->orderBy('po_id','DESC')
                ->paginate($limit,$page);

        $result->map(function($res){
            // $warna = "#0089C7";//biru
            // $res['doc_status'] = $res->doc_status?$res->doc_status:"";
            // $res['doc_position'] = $res->doc_position?$res->doc_position:"";
            // $res['received_date'] = $res->received_date?$res->received_date:"";
            // $res['submited_date'] = $res->submited_date?$res->submited_date:"";
            // $res['verification_time'] = "";
            // $res['status_color'] = $warna;
            $res['doc'] = "po";
        
            return $res;
        });

        return $result;
    }

    public function scanPO($id){
        $result = DB::table("po")
                ->select('po_id','po_num','description AS title','ts.status','ts.workflow AS doc_position','ts.received_date','ts.submited_date',DB::raw('IF(ts.received_date IS NULL,"received","submit") action'))
                ->leftJoin('tracking_status as ts','ts.source_id','=','po.po_id')
                ->where('po_id',$id)
                ->orderBY('id','DESC')
                ->first();
        /*$result = DB::select("
            SELECT ts.id,pr.pr_id,pr.pr_num,pr.description AS title,ts.status,ts.workflow doc_position,ts.received_date,ts.submited_date,IF(ts.received_date IS NULL,'received','submit') action
            FROM pr 
            LEFT JOIN tracking_status as ts ON ts.source_id=pr.pr_id
        
            WHERE pr_id=".$id."
            ORDER BY id DESC
            LIMIT 1
        ");*/
        return $result;
    }

    public function scanNumPO($num){
        $result = DB::table("po")
                ->select('po_id','po_num','description AS title','po.status','po.doc_position','po.routing_instruction as instruction','po.memo','ts.submited_date',DB::raw('po.received_date as received_date, IF(po.received_date IS NULL,"received","submit") as action'))
                ->leftJoin('tracking_status as ts', function($join) {
                    $join->on('po.po_id', '=', 'ts.source_id');
                    $join->on('po.doc', '=', 'ts.source_doc');
                })
                ->where('po.po_num',$num)
                ->orderBY('ts.id','DESC')
                ->first();
       
        return $result;
    }

    public function getPOByID($id){
        $result = DB::table("po")
        ->select('po_id as id','po_num as num','description AS title','status','doc_status','doc_position','process_owner_id as owner_id','process_owner_nip as owner_nip','received_date','submited_date')
                ->where('po_id',$id)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getPOByNUM($num){
        $result = DB::table("po")
        ->select('po_id as id','po_num as num','description AS title','status','doc_status','doc_position','received_date','submited_date','process_owner_id','process_owner_nip')
                ->where('po_num',$num)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getLastTrackingStatus($id,$doc){
        $result = DB::table("tracking_status as ts")
                ->leftJoin('workflow as w','w.workflow_id','=','ts.workflow_id')
                ->select('ts.*','w.num_flow')
                ->where('source_id',$id)
                ->where('source_doc',$doc)
                ->orderBy('id','DESC')
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function checkWorkflowUser($nip,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('wu.user_nip',$nip)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getNextWorkflow($num_flow,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('w.num_flow',$num_flow)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function receivedPO($req){
        ///return response()->json($req->id);
        $po = $this->getPOByID($req->id);
        $user = auth()->user();
        $last_status = $this->getLastTrackingStatus($req->id,"po");
        if($last_status){

            $next_ = $last_status->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"po");

            $data = array(
                'status'        => @$po->status,
                'received_date' => date('Y-m-d H:i:s'),
                'updated_by'    => $user->nip
            );

            if($last_status->user_nip=="" OR $last_status->workflow==""){
                $data['user_nip'] = $po->owner_nip;
            }
            $query = DB::table('tracking_status')
                    ->where('id',$last_status->id)
                    ->update($data);
        }else{

            $check_wu = $this->checkWorkflowUser($user->nip,"po");
            $next_ = $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"po");

            $insert = array(
                'source_id'     => $req->id,
                'source_doc'    => "po",
                'user_id'       => $user->user_id,
                'user_nip'      => $user->nip,
                //'status'        => $check_wu->status,
                'status'        => "On-Progres",
                'workflow'      => $check_wu->workflow_name,
                'workflow_id'   => $check_wu->workflow_id,
                'keterangan'    => $req->note,
                'sent_by'       => $user->nip,
                'sent_date'     => date('Y-m-d H:i:s'),
                'received_date' => date('Y-m-d H:i:s'),
                //'submited_date' => date('Y-m-d H:i:s'),
                'created_by'    => date('Y-m-d H:i:s'),
            );

            $query = DB::table('tracking_status')->insert($insert);

            
        }
        $data_po = array(
            //'status'=>'PRINPG',
            //'doc_status'=>'PRINPG',
            'received_date'=>date('Y-m-d H:i:s')
        );

        $update = DB::table('po')
                    ->where('po_id',$req->id)
                    ->update($data_po);

        if($query){
            $return = true;
        }else{
            $return = false;
        }
    }

    /*public function submitPO($req){
        $verifikasi = $req->verifikasi;
        $pr = $this->getPOByID($req->id);
        $user = auth()->user();

        if(strtolower($pr->status)=="closed"){
            return false;
        }

        if($pr){

            $last_status = $this->getLastTrackingStatus($req->id,"po");
            // jika belum ada doc d tracking_status
            $check_wu = $this->checkWorkflowUser($user->nip,"po");
            $next_ = $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"po");

            if($req->verifikasi==1 OR $req->verifikasi==3){
                // jika verifikasi ok 
                if($last_status){
                    $next_ = $last_status->num_flow+1;
                    $next_flow = $this->getNextWorkflow($next_,"po");

                    if($last_status->num_flow==1){
                        $status_sub = "Send";
                    }else{
                        $status_sub = "Approved";
                    }

                    $data = array(
                        'status'        => $status_sub,
                        'keterangan'    => $req->note,
                        'submited_date' => date('Y-m-d H:i:s'),
                        'updated_by'    => $user->nip
                    );
                    //update submited
                    $update = DB::table('tracking_status')
                            ->where('id',$last_status->id)
                            ->update($data);

                    if($last_status->num_flow==1 AND $last_status->revisi==1){
                        $status = "RESEND";
                    }else{
                        $status = "Waiting Approval";
                    }

                    $workflow_id = $next_flow->workflow_id;

                    // insert for next status        
                    $insert_next = array(
                        'source_id'     => $req->id,
                        'source_doc'    => "po",
                        'user_id'       => $next_flow->user_id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => "Waiting Approval",
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status')->insert($insert_next);
                    if($query){
                        $return = true;
                        $position = $next_flow->workflow_name;
                        $user_nip = $next_flow->user_nip;
                    }else{
                        $return = false;
                    }
                    //return $last_status;
                }else{
                    
                    //insert first to tracking status
                    $insert = array(
                        'source_id'     => $req->id,
                        'source_doc'    => "po",
                        'user_id'       => $user->user_id,
                        'user_nip'      => $user->nip,
                        'status'        => $check_wu->status,
                        'workflow'      => $check_wu->workflow_name,
                        'workflow_id'   => $check_wu->workflow_id,
                        'keterangan'    => $req->note,
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        'received_date' => date('Y-m-d H:i:s'),
                        'submited_date' => date('Y-m-d H:i:s'),
                        'created_by'    => date('Y-m-d H:i:s'),
                    );

                    DB::table('tracking_status')->insert($insert);
                    $status = "Waiting Approval";

                    $workflow_id = $next_flow->workflow_id;

                    // insert for next status
                    $insert_next = array(
                        'source_id'     => $req->id,
                        'source_doc'    => "po",
                        'user_id'       => $next_flow->user_id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => $status,
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status')->insert($insert_next);
                    if($query){
                        $return = true;
                        $position = $next_flow->workflow_name;
                        $user_nip = $next_flow->user_nip;
                    }else{
                        $return = false;
                    }
                }
            }else{
                // jika di tolak/revisi
                if($last_status->num_flow>5){
                    $revisi = "PLNREVISE";
                    $next_ = 5;
                    $next_flow = $this->getNextWorkflow($next_,"po");
                }
                else if($last_status->num_flow==5){
                    $revisi = "PLNREVISE";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"po");
                }
                else{
                    $revisi = "MODIFY";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"po");
                }
                if($req->verifikasi=='1'){
                    $status = "Reject";
                }else{
                    $status = $revisi;
                }
                
                $data = array(
                    'status'        => $status,
                    'keterangan'    => $req->note,
                    'submited_date' => date('Y-m-d H:i:s'),
                    'updated_by'    => $user->nip,
                    'revisi'        => 1
                );
                //update submited
                $update = DB::table('tracking_status')
                        ->where('id',$last_status->id)
                        ->update($data);

                $workflow_id = $next_flow->workflow_id;

                $insert_next = array(
                    'source_id'     => $req->id,
                    'source_doc'    => "po",
                    'user_id'       => $next_flow->user_id,
                    'user_nip'      => $next_flow->user_nip,
                    'status'        => $status,
                    'workflow'      => $next_flow->workflow_name,
                    'workflow_id'   => $next_flow->workflow_id,
                    'keterangan'    => $req->note,
                    'sent_by'       => $user->nip,
                    'sent_date'     => date('Y-m-d H:i:s'),
                    //'received_date' => "",
                    //'submited_date' => "",
                    'created_by'    => date('Y-m-d H:i:s'),
                    'revisi'        => 1
                );
                $query = DB::table('tracking_status')->insert($insert_next);
                if($query){
                    $return = true;
                    $position = $next_flow->workflow_name;
                    $user_nip = $next_flow->user_nip;
                }else{
                    $return = false;
                }

            }
            
        }else{
            $return = false;
        }
        $data_po = array(
            'status'=>$status,
            'doc_status'=>$status,
            'doc_position' => $position,
            'process_owner_nip' => $user_nip,
            'sent_date' => date('Y-m-d H:i:s'),
            'received_date' => null,
            //'submited_date'=>date('Y-m-d H:i:s')
        );

        $update = DB::table('po')
                    ->where('po_id',$req->id)
                    ->update($data_po);

        if($update){

            $title="Dokumen Masuk PO NO : ".$pr->num;
            $pesan ="PO NO. ".$pr->num." telah dikirim ke meja Bapak/Ibu, harap untuk segera diterima. \n --Admin TJBSC.";
            $this->sendPushNotification($user->token_fcm,$title,$pesan);
            $workflow = $this->getWorkFlowUserID($workflow_id);
            foreach ($workflow as $key) {
                $data_msg = array(
                    'sent_by'       => $user->nip,
                    'receipt_by'    => $key->user_nip,
                    'title'         => $title,
                    'message'       => $pesan,
                    'created_by'    => $user->user_id,
                    'created_date'  => date('Y-m-d H:i:s')
                );
                $this->sendMessage($data_msg);
            }
            
        }
        return $return;
    }*/

    public function closePO($req){
        $id = $req->id;
        $user = auth()->user();
        $last_status = $this->getLastTrackingStatus($req->id,"po");
        $data = array(
            'status'        => "Closed",
            'keterangan'    => "PO SENT",
            'submited_date' => date('Y-m-d H:i:s'),
            'updated_by'    => $user->nip
        );
        //update submited
        $update = DB::table('tracking_status')
                ->where('id',$last_status->id)
                ->update($data);

        $data_po = array(
            'status'        => "Closed",
            'doc_status'    => "Closed",
            'submited_date' => date('Y-m-d H:i:s')
        );

        $update_po = DB::table('po')
                    ->where('po_id',$req->id)
                    ->update($data_po);

        return $update_po;
    }

    public function createPOMaximo($req){
        $po = $this->getPOByNum($req->po_num);
        $data_po = array(
            'po_num'                    => $req->po_num,
            'budget_categories'         => $req->budget_categories,
            //'asset'                     => $req->asset,
            'description'               => $req->description,
            'total_cost'                => $req->total_cost,
            'status'                    => @$req->status?$req->status:"Created",
            'status_description'        => @$req->status_description?$req->status_description:"",
            'doc_status'                => @$req->status?$req->status:"Created",
            'doc_position'              => @$req->position?$req->position:"Buyer",
            'remarks'                   => @$req->remarks?$req->remarks:"",
            'routing_instruction'       => @$req->routing_instruction?$req->routing_instruction:"",
            'process_owner_id'          => @$req->ownerid,
            'process_owner_nip'         => @$req->ownernip,
            'ordered_date'              => $req->ordered_date
        );
        if($po){
            if(strtolower($req->status)=='concur'){
                $data_po['ae_rendal'] = date('Y-m-d H:i:s');
            }
            $data_po['received_date']         = NULL;
            $query = DB::table('po')->where('po_num',$req->po_num)->update($data_po);
            $last = $this->getLastTrackingStatus($po->id,"po");
            if($last->user_nip!=@$req->ownernip){
                
                $data = array(
                    'submited_date'   => date('Y-m-d H:i:s'),
                    'updated_by'      => @$req->ownernip,
                );
                if($last->user_nip=="" OR $last->workflow==""){
                    $data['user_nip'] = @$po->process_owner_nip;
                }
                //update submited
                $update = DB::table('tracking_status')
                        ->where('id',$last->id)
                        ->update($data);
                
                $insert_ = array(
                    'source_id'     => $po->id,
                    'source_num'    => $req->po_num,
                    'source_doc'    => "po",
                    'user_nip'      => @$req->ownernip,
                    'status'        => $req->status,
                    'workflow'      => $req->position,
                    'workflow_id'   => "",
                    'keterangan'    => @$req->note,
                    'sent_by'       => "",
                    'sent_date'     => date('Y-m-d H:i:s'),
                    'created_by'    => date('Y-m-d H:i:s'),
                );
                if(@$req->ownernip!=""){
                    $query = DB::table('tracking_status')->insert($insert_);
                }
                
            }
        }else{
            $data_po['created_by'] = $req->created_by;
            $data_po['created_date'] = date('Y-m-d H:i:s');
            $data_po['sent_date'] = date('Y-m-d H:i:s');
            $query = DB::table('po')->insert($data_po);
            $po_id = DB::getPdo()->lastInsertId();
            $insert_ = array(
                'source_id'     => $po_id,
                'source_num'    => $req->po_num,
                'source_doc'    => "po",
                'user_nip'      => @$req->ownernip,
                'status'        => @$req->status,
                'workflow'      => @$req->position,
                'workflow_id'   => "",
                'keterangan'    => @$req->note,
                'sent_by'       => "",
                'sent_date'     => date('Y-m-d H:i:s'),
                'received_date' => date('Y-m-d H:i:s'),
                'created_by'    => date('Y-m-d H:i:s'),
            );
            $query = DB::table('tracking_status')->insert($insert_);
        }
        

        if($query){
            
            return true;
        }else{
            return false;
        }
         
    }

    public function updatePOMaximo($req){
        
        $data_po = array(
            'po_num'                    => $req->po_num,
            'budget_categories'         => $req->budget_categories,
            //'asset'                     => $req->asset,
            'description'               => $req->description,
            'total_cost'                => $req->total_cost,
            'status'                    => @$req->status?$req->status:"Created",
            'status_description'        => @$req->status_description?$req->status_description:"",
            'doc_status'                => @$req->status?$req->status:"Created",
            'doc_position'              => @$req->position?$req->position:"Buyer",
            'remarks'                   => @$req->remarks?$req->remarks:"",
            'routing_instruction'       => @$req->routing_instruction?$req->routing_instruction:"",
            'process_owner_id'          => @$req->ownerid,
            'process_owner_nip'         => @$req->ownernip,
            'ordered_date'              => $req->ordered_date,
            'updated_by'                => $req->created_by,
            'updated_date'              => date('Y-m-d H:i:s')
        );
        $query = DB::table('po')->where('po_num',$req->po_num)->update($data_po);

        if($query){
            return true;
        }else{
            return false;
        }
         
    }

    public function RoutingPOMaximo($req){
        $po = $this->getPOByNum($req->po_num);

        $data_po = array(
            'status'                => $req->status,
            'doc_status'            => $req->status,
            'status_description'    => $req->status_description,
            'doc_position'          => $req->position,
            'process_owner_id'      => $req->ownerid,
            'process_owner_nip'     => $req->ownernip,
            //'remarks'               => @$req->note,
            'routing_instruction'   => $req->routing_instruction,
            'sent_date'             => date('Y-m-d H:i:s'),
            //'submited_date'         => date('Y-m-d H:i:s')
            'received_date'         => NULL
        );
        if($po){

            if(strtolower($req->status)=='concur'){
                $data_pr['ae_rendal'] = date('Y-m-d H:i:s');
            }else if(strtolower($req->status)=='concur1'){
                $data_pr['msb_log'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='concur2'){
                $data_pr['srm_produksi'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='srmreview'){
                $data_pr['gm'] = date('Y-m-d H:i:s');
            }
            else if(strtolower($req->status)=='srmappr' OR strtolower($req->status)=='gmappr'){
                $data_pr['ae_rendal2'] = date('Y-m-d H:i:s');
            }

            $submit_po = DB::table('po')
                        ->where('po_num',$req->po_num)
                        ->update($data_po);
            $po_id  = $po->id;
            
        }else{
            $data_po['po_num'] = $req->po_num;
            $data_po['description'] = @$req->description;
            $data_po['created_date'] = date('Y-m-d H:i:s');

            $submit_po = DB::table('po')->insert($data_po);
            $po_id = DB::getPdo()->lastInsertId();
        }

        if($submit_po){

            $last_status = $this->getLastTrackingStatus($po_id,"po");
            if($last_status){
                
                $data = array(
                    //'status'        => $req->status,
                    //'user_nip'      => @$req->ownernip,
                    //'keterangan'    => @$req->note,
                    'submited_date'   => date('Y-m-d H:i:s'),
                    'updated_by'      => @$req->ownernip,
                );
                if($last_status->user_nip=="" OR $last_status->workflow==""){
                    $data['user_nip'] = @$po->process_owner_nip;
                }
                //update submited
                $update = DB::table('tracking_status')
                        ->where('id',$last_status->id)
                        ->update($data);
            }else{
                
                $insert_ = array(
                    'source_id'     => $po_id,
                    'source_num'    => $req->po_num,
                    'source_doc'    => "po",
                    'user_nip'      => @$req->ownernip,
                    'status'        => $req->status,
                    'workflow'      => $req->position,
                    'workflow_id'   => "",
                    'keterangan'    => @$req->note,
                    'sent_by'       => "",
                    'sent_date'     => date('Y-m-d H:i:s'),
                    'received_date' => date('Y-m-d H:i:s'),
                    'submited_date' => date('Y-m-d H:i:s'),
                    'created_by'    => date('Y-m-d H:i:s'),
                );
                $query = DB::table('tracking_status')->insert($insert_);
            }
           
            $insert_next = array(
                'source_id'     => $po_id,
                'source_num'    => $req->po_num,
                'source_doc'    => "po",
                'user_nip'      => @$req->ownernip,
                //'status'        => "Waiting Received",
                'status'        => @$req->status,
                'workflow'      => @$req->position,
                'workflow_id'   => "",
                //'keterangan'    => "",
                'sent_by'       => @$po->process_owner_nip,
                'sent_date'     => date('Y-m-d H:i:s'),
                //'received_date' => "",
                //'submited_date' => "",
                'created_by'    => date('Y-m-d H:i:s'),
            );
            $query = DB::table('tracking_status')->insert($insert_next); 

            $user = DB::table('sys_users')->where('nip',@$req->ownernip)->first();
            if($user){
                $title="Masuk PO NO : ".$po->num;
                $pesan ="PO NO. ".$po->num." telah dikirim, harap untuk segera diterima.";
                $this->sendPushNotification($user->token_fcm,$title,$pesan);
                $data_msg = array(
                    'sent_by'       => @$po->process_owner_nip,
                    'receipt_by'    => @$user->nip,
                    'title'         => $title,
                    'message'       => $pesan,
                    'created_by'    => @$po->process_owner_nip,
                    'created_date'  => date('Y-m-d H:i:s')
                );
                $this->sendMessage($data_msg);
                $this->createLogResponseMaximo(@$req->ownernip,"send inbox",json_encode($data_msg));
            }
            

        }
        
    }

    

}

?>