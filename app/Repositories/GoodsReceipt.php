<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\GoodReceiptModel;
use App\Repositories\Api;

class GoodsReceipt extends Api{
    
    public function getGoodsReceipt($page,$req) {
        $return = array();
        $limit = @$req->limit?@$req->limit:20;
        $search = @$req->search?@$req->search:"";
        $doc = 'gr';
        $result = GoodReceiptModel::
                select('gr.id','gr.po_num as num','gr.description AS title','gr.routing_instruction as instruction','gr.sent_date','st.received_date','st.submited_date','gr.created_date', DB::raw('IFNULL(gr.doc_status,st.status) status,IFNULL(gr.doc_position,st.workflow) doc_position,DATEDIFF(now(),IFNULL(gr.sent_date,gr.created_date)) as days'))
                ->leftJoin(DB::raw('(SELECT source_id,source_doc, status, workflow,received_date, submited_date from `tracking_status` group by id  order by id ASC) AS st'), function($join) {
                    $join->on('gr.id', '=', 'st.source_id');
                    $join->on('gr.doc', '=', 'st.source_doc');
                })
                ->when($search, function ($query, $search) {
                     return $query->where('po_num','like','%'.$search.'%');
                })
                ->when($search, function ($query, $search) {
                    return $query->orWhere('description','like','%'.$search.'%');
                })
                ->groupBy('gr.id')
                ->paginate($limit,$page);
            //->toSql();
        $result->map(function($res){

            if($res->id=='664737016'){
                $warna = "#dc143c";//merah
            }else if($res->id=='664738723'){
                $warna = "#f0f000";//kuning
            }
            else{
                
                $warna = "#0089C7";//biru
            }
            $warna = "#0089C7";//biru
            $res['status'] = $res->status?$res->status:"";
            $res['doc_position'] = $res->doc_position?$res->doc_position:"";
            $res['instruction'] = $res->instruction?$res->instruction:"Please review this doc";
            $res['sent_date'] = $res->sent_date?date('d M Y',strtotime($res->sent_date)):"-";
            $res['received_date'] = $res->received_date?$res->received_date:"";
            $res['submited_date'] = $res->submited_date?$res->submited_date:"";
            $res['created_date'] = $res->created_date?date('d M Y',strtotime($res->created_date)):"";
            $res['verification_time'] = $res->days?$res->days:"-";
            $res['status_color'] = $warna;
            $res['doc'] = "gr";
        
            return $res;
        });
        
        $data = array(
            'data'      => $result['data'],
            //'last_page' => $result->last_page,
            //'total'     => $result->total
        );
        return $result;
    }

    public function getGRByID($id){
        $result = DB::table("gr")
        ->select('id','po_num as num','description AS title','doc_status','doc_position','routing_instruction as instruction','memo','sent_date','received_date','submited_date','created_date')
                ->where('id',$id)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getGRByNUM($num){
        $result = DB::table("gr")
        ->select('id','po_num as num','description AS title','doc_status','doc_position','received_date','submited_date')
                ->where('po_num',$num)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getLastTrackingStatus($id,$doc){
        $result = DB::table("tracking_status as ts")
                ->leftJoin('workflow as w','w.workflow_id','=','ts.workflow_id')
                ->select('ts.*','w.num_flow')
                ->where('source_id',$id)
                ->where('source_doc',$doc)
                ->orderBy('id','DESC')
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function checkWorkflowUser($nip,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('wu.user_nip',$nip)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function getNextWorkflow($num_flow,$doc){
        $result = DB::table("workflow_user as wu")
                ->leftJoin('workflow as w','w.workflow_id','=','wu.workflow_id')
                ->select('wu.*','w.workflow_name','w.num_flow','w.status')
                ->where('w.num_flow',$num_flow)
                ->where('w.doc',$doc)
                ->first();

        if($result){
            return $result;
        }
        return false;
    }

    public function scanGR($id){
        $result = DB::table("gr")
                ->select('gr.id','gr.po_num as num','gr.description AS title','ts.status','ts.workflow AS doc_position','ts.received_date','ts.submited_date',DB::raw('IF(ts.received_date IS NULL,"received","submit") action'))
                ->leftJoin('tracking_status as ts', function($join) {
                    $join->on('gr.id', '=', 'ts.source_id');
                    $join->on('gr.doc', '=', 'ts.source_doc');
                })
                ->where('gr.id',$id)
                ->orderBY('ts.id','DESC')
                ->first();
        /*$result = DB::select("
            SELECT ts.id,pr.pr_id,pr.pr_num,pr.description AS title,ts.status,ts.workflow doc_position,ts.received_date,ts.submited_date,IF(ts.received_date IS NULL,'received','submit') action
            FROM pr 
            LEFT JOIN tracking_status as ts ON ts.source_id=pr.pr_id
        
            WHERE pr_id=".$id."
            ORDER BY id DESC
            LIMIT 1
        ");*/
        return $result;
    }

    public function scanNumGR($id){
        $result = DB::table("gr")
                ->select('gr.id','gr.po_num as num','gr.description AS title','gr.doc_status as status','gr.doc_position','gr.routing_instruction as instruction','gr.memo','ts.submited_date',DB::raw('IFNULL(gr.received_date,ts.received_date) as received_date,IF(ts.received_date IS NULL,"received","submit") action'))
                ->leftJoin('tracking_status as ts', function($join) {
                    $join->on('gr.id', '=', 'ts.source_id');
                    $join->on('gr.doc', '=', 'ts.source_doc');
                })
                ->where('gr.po_num',$id)
                ->orderBY('ts.id','DESC')
                ->first();
        return $result;
    }

    public function receivedGR($req){
        $user = auth()->user();
        $last_status = $this->getLastTrackingStatus($req->id,"gr");
        if($last_status){

            $next_ = $last_status->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"gr");

            $data = array(
                'status'        => 'On-Progress',
                'received_date' => date('Y-m-d H:i:s'),
                'updated_by'    => $user->nip
            );
            $query = DB::table('tracking_status')
                    ->where('id',$last_status->id)
                    ->update($data);
        }else{

            $check_wu = $this->checkWorkflowUser($user->nip,"gr");
            $next_ = $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"gr");

            $insert = array(
                'source_id'     => $req->id,
                'source_num'    => $req->num,
                'source_doc'    => "gr",
                'user_id'       => $user->user_id,
                'user_nip'      => $user->nip,
                //'status'        => $check_wu->status,
                'status'        => "On-Progress",
                'workflow'      => $check_wu->workflow_name,
                'workflow_id'   => $check_wu->workflow_id,
                'keterangan'    => $req->note,
                'sent_by'       => $user->nip,
                'sent_date'     => date('Y-m-d H:i:s'),
                'received_date' => date('Y-m-d H:i:s'),
                //'submited_date' => date('Y-m-d H:i:s'),
                'created_by'    => date('Y-m-d H:i:s'),
            );

            $query = DB::table('tracking_status')->insert($insert);

            
        }
        $data_pr = array(
            'doc_status'=>'On-Progress',
            'received_date'=>date('Y-m-d H:i:s')
        );

        $update = DB::table('gr')
                    ->where('id',$req->id)
                    ->update($data_pr);

        if($query){
            $return = true;
        }else{
            $return = false;
        }
    }

    public function submitGR($req){
        $verifikasi = $req->verifikasi;
        $gr = $this->getGRByID($req->id);
        $user = auth()->user();
        if($gr){
            $last_status = $this->getLastTrackingStatus($req->id,"gr");
            // jika belum ada doc d tracking_status
            $check_wu = $this->checkWorkflowUser($user->nip,"gr");
            $next_ = (int) $check_wu->num_flow+1;
            $next_flow = $this->getNextWorkflow($next_,"gr");
            if(empty($next_flow)){
                return false;
            }
            if($req->verifikasi==1 OR $req->verifikasi==3){
                // jika verifikasi ok 
                if($last_status){
                    $next_ = (int) $last_status->num_flow+1;
                    $next_flow = $this->getNextWorkflow($next_,"gr");

                    if($last_status->num_flow==1){
                        $status_sub = "Send";
                    }else{
                        $status_sub = "Approved";
                    }

                    $data = array(
                        'status'        => $status_sub,
                        'keterangan'    => $req->note,
                        'submited_date' => date('Y-m-d H:i:s'),
                        'updated_by'    => $user->nip
                    );
                    //update submited
                    $update = DB::table('tracking_status')
                            ->where('id',$last_status->id)
                            ->update($data);

                    if($last_status->num_flow==1 AND $last_status->revisi==1){
                        $status = "RESEND";
                    }else{
                        $status = "Waiting Approval";
                    }
                    // insert for next status        
                    $insert_next = array(
                        'source_id'     => $req->id,
                        'source_num'    => $req->num,
                        'source_doc'    => "gr",
                        'user_id'       => $next_flow->user_id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => "Waiting Approval",
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status')->insert($insert_next);
                    if($query){
                        $return = true;
                    }else{
                        $return = false;
                    }
                    //return $last_status;
                }else{
                    $status = "Waiting Approval";
                    //insert first to tracking status
                    $insert = array(
                        'source_id'     => $req->id,
                        'source_num'    => $req->num,
                        'source_doc'    => "gr",
                        //'user_id'       => $user->user_id,
                        'user_nip'      => $user->nip,
                        'status'        => $check_wu->status,
                        'workflow'      => $check_wu->workflow_name,
                        'workflow_id'   => $check_wu->workflow_id,
                        'keterangan'    => $req->note,
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        'received_date' => date('Y-m-d H:i:s'),
                        'submited_date' => date('Y-m-d H:i:s'),
                        'created_by'    => date('Y-m-d H:i:s'),
                    );

                    DB::table('tracking_status')->insert($insert);

                    // insert for next status
                    $insert_next = array(
                        'source_id'     => $req->id,
                        'source_num'    => $req->num,
                        'source_doc'    => "gr",
                        //'user_id'       => $next_flow->user_id,
                        'user_nip'      => $next_flow->user_nip,
                        //'status'        => $next_flow->status,
                        'status'        => $status,
                        'workflow'      => $next_flow->workflow_name,
                        'workflow_id'   => $next_flow->workflow_id,
                        //'keterangan'    => "",
                        'sent_by'       => $user->nip,
                        'sent_date'     => date('Y-m-d H:i:s'),
                        //'received_date' => "",
                        //'submited_date' => "",
                        'created_by'    => date('Y-m-d H:i:s'),
                    );
                    $query = DB::table('tracking_status')->insert($insert_next);
                    if($query){
                        $return = true;
                    }else{
                        $return = false;
                    }
                }
            }else{
                // jika di tolak/revisi
                if($last_status->num_flow>5){
                    $revisi = "PLNREVISE";
                    $next_ = 5;
                    $next_flow = $this->getNextWorkflow($next_,"gr");
                }
                else if($last_status->num_flow==5){
                    $revisi = "PLNREVISE";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"gr");
                }
                else{
                    $revisi = "MODIFY";
                    $next_ = 1;
                    $next_flow = $this->getNextWorkflow($next_,"gr");
                }
                if($req->verifikasi=='1'){
                    $status = "Reject";
                }else{
                    $status = $revisi;
                }
                
                $data = array(
                    'status'        => $status,
                    'keterangan'    => $req->note,
                    'submited_date' => date('Y-m-d H:i:s'),
                    'updated_by'    => $user->nip,
                    'revisi'        => 1
                );
                //update submited
                $update = DB::table('tracking_status')
                        ->where('id',$last_status->id)
                        ->update($data);

                $insert_next = array(
                    'source_id'     => $req->id,
                    'source_num'    => $req->num,
                    'source_doc'    => "gr",
                    'user_id'       => $next_flow->user_id,
                    'user_nip'      => $next_flow->user_nip,
                    'status'        => $status,
                    'workflow'      => $next_flow->workflow_name,
                    'workflow_id'   => $next_flow->workflow_id,
                    'keterangan'    => $req->note,
                    'sent_by'       => $user->nip,
                    'sent_date'     => date('Y-m-d H:i:s'),
                    //'received_date' => "",
                    //'submited_date' => "",
                    'created_by'    => date('Y-m-d H:i:s'),
                    'revisi'        => 1
                );
                $query = DB::table('tracking_status')->insert($insert_next);
                if($query){
                    $return = true;
                }else{
                    $return = false;
                }

            }
            
        }else{
            $return = false;
        }
        $data_gr = array(
            'doc_status'=>$status,
            'submited_date'=>date('Y-m-d H:i:s')
        );

        $update = DB::table('gr')
                    ->where('id',$req->id)
                    ->update($data_gr);
        return $return;
    }


    

}

?>