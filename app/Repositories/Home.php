<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\PurchaseOrder;
use App\Model\InboxModel;
use App\Repositories\Api;

class Home extends Api{

    public function unreadTotal() {
        $return = array();
        $user = auth()->user();
        $result = InboxModel::
                select('title','message','is_read','created_date')
                ->where('is_read','0')
                ->where('receipt_by',$user->nip)
                ->count();
            //->toSql();
        
        return $result;
    }

    public function getDashboard(){
        $return = array();
        $info_terkini = array();
        $user = auth()->user();
        $nip = $user->nip;
        $dashboard = DB::select("select user_nip,count(*) total,
        IFNULL((SELECT count(*) FROM pr WHERE process_owner_nip=? AND received_date IS NULL ),0) wr_pr,
        IFNULL((SELECT count(*) FROM pr WHERE process_owner_nip=? AND received_date IS NOT NULL ),0) rec_pr,
        IFNULL((SELECT count(*) FROM po WHERE process_owner_nip=? AND received_date IS NULL ),0) wr_po,
        IFNULL((SELECT count(*) FROM po WHERE process_owner_nip=? AND received_date IS NOT NULL ),0) rec_po,
        IFNULL((SELECT count(*) FROM gr WHERE process_owner_nip=? AND received_date IS NULL ),0) wr_gr,
        IFNULL((SELECT count(*) FROM gr WHERE process_owner_nip=? AND received_date IS NOT NULL ),0) rec_gr,
        IFNULL((SELECT count(user_nip) FROM tracking_status WHERE user_nip=? AND received_date IS NOT NULL AND submited_date IS NOT NULL group by source_num limit 1),0) approved
        FROM tracking_status where user_nip=? group by user_nip limit 1",[$nip,$nip,$nip,$nip,$nip,$nip,$nip,$nip]);
        
        $dashboard_invoice = DB::select("select user_nip,count(*) total,
        IFNULL((SELECT count(user_nip) FROM tracking_status_invoice WHERE user_nip=? AND received_date IS NULL AND submited_date IS NULL),0) wappr,
        IFNULL((SELECT count(user_nip) FROM tracking_status_invoice WHERE user_nip=? AND received_date IS NOT NULL AND submited_date IS NULL),0) inprogress,
        IFNULL((SELECT count(user_nip) FROM tracking_status_invoice WHERE user_nip=? AND received_date IS NOT NULL AND submited_date IS NOT NULL),0) approved
        
        FROM tracking_status_invoice where user_nip=? group by user_nip limit 1",[$nip,$nip,$nip,$nip]);
        
        $info = DB::select("SELECT
                    ts.source_id,
                    ts.source_doc,
                    IFNULL(pr.pr_num,IFNULL(po.po_num,gr.po_num)) num,
                    IFNULL(pr.description,IFNULL(po.description,gr.Description)) title,
                    ts.sent_date,
                    now() as now,
                    ts.received_date,
                    ts.submited_date,
                    wk.baku_waktu,
                ROUND(time_to_sec((TIMEDIFF(IFNULL(ts.submited_date,now()),IFNULL(ts.received_date,ts.sent_date) ))) / 60) menit	
                FROM
                    tracking_status ts
                    LEFT JOIN workflow wk ON wk.workflow_id=ts.workflow_id
                    LEFT JOIN pr ON pr.pr_id=ts.source_id AND source_doc='pr'
                    LEFT JOIN po ON po.po_id=ts.source_id AND source_doc='po'
                    LEFT JOIN gr ON gr.id=ts.source_id AND source_doc='gr'
                WHERE
                    ts.user_nip = ?
                    AND ( ts.received_date IS NULL OR ts.submited_date IS NULL)
                    ORDER BY ts.id DESC
                    ",[$nip]);

                $info_pr = DB::select("SELECT
                pr.pr_id as id,
                pr.doc,
                wk.doc wkdoc,
                pr.pr_num as num,
                pr.description as title,
                pr.routing_instruction as routing_instruction,
                pr.status as doc_status,
                pr.doc_position,
                u.position,
                pr.sent_date,
                now() as now,
                pr.received_date,
                pr.submited_date,
                pr.date_created created_date,
                IFNULL(wk.baku_waktu,3) baku_waktu,
                ROUND(time_to_sec((TIMEDIFF(now(),pr.sent_date ))) / 60) menit,
                DATEDIFF(now(),IFNULL(pr.sent_date,pr.date_created)) as days,
                IF(pr.received_date IS NULL,'Waiting Received','On Progress') label	
                FROM
                pr
                LEFT JOIN sys_users as u ON u.nip=pr.process_owner_nip
                LEFT JOIN workflow_user as wu ON wu.user_nip=pr.process_owner_nip
                LEFT JOIN workflow as wk ON wk.workflow_id=wu.workflow_id

                WHERE
                pr.process_owner_nip = ?
                AND pr.doc='pr'
                group by pr.pr_id
                ",[$nip]);

                $info_po = DB::select("SELECT
                po.po_id as id,
                po.doc,
                wk.doc wkdoc,
                po.po_num as num,
                po.description as title,
                po.routing_instruction as routing_instruction,
                po.status as doc_status,
                po.doc_position,
                u.position,
                po.sent_date,
                now() as now,
                po.received_date,
                po.submited_date,
                po.created_date created_date,
                IFNULL(wk.baku_waktu,3) baku_waktu,
                ROUND(time_to_sec((TIMEDIFF(now(),po.sent_date ))) / 60) menit,
                DATEDIFF(now(),IFNULL(po.sent_date,po.created_date)) as days,
                IF(po.received_date IS NULL,'Waiting Received','On Progress') label	
                FROM
                po
                LEFT JOIN sys_users as u ON u.nip=po.process_owner_nip
                LEFT JOIN workflow_user as wu ON wu.user_nip=po.process_owner_nip
                LEFT JOIN workflow as wk ON wk.workflow_id=wu.workflow_id

                WHERE
                po.process_owner_nip = ?
                AND po.doc='po'
                group by po.po_id
                ",[$nip]);

                $info_gr = DB::select("SELECT
                gr.id,
                gr.doc,
                wk.doc wkdoc,
                gr.po_num as num,
                gr.description as title,
                gr.routing_instruction as routing_instruction,
                gr.doc_status,
                gr.doc_position,
                u.position,
                gr.sent_date,
                now() as now,
                gr.received_date,
                gr.submited_date,
                gr.created_date created_date,
                IFNULL(wk.baku_waktu,3) baku_waktu,
                ROUND(time_to_sec((TIMEDIFF(now(),gr.sent_date ))) / 60) menit,
                DATEDIFF(now(),IFNULL(gr.sent_date,gr.created_date)) as days,
                IF(gr.received_date IS NULL,'Waiting Received','On Progress') label	
                FROM
                gr
                LEFT JOIN sys_users as u ON u.nip=gr.process_owner_nip
                LEFT JOIN workflow_user as wu ON wu.user_nip=gr.process_owner_nip
                LEFT JOIN workflow as wk ON wk.workflow_id=wu.workflow_id

                WHERE
                gr.process_owner_nip = ?
                AND gr.doc='gr'
                group by gr.id
                ",[$nip]);

        $info_invoice = DB::select("SELECT
                ts.invoice_id source_id,
                'invoice' source_doc,
                inv.num,
                inv.text title,
                ts.sent_date,
                now() as now,
                ts.received_date,
                ts.submited_date,
                wk.baku_waktu,
                ROUND(time_to_sec((TIMEDIFF(IFNULL(ts.submited_date,now()),IFNULL(ts.received_date,ts.sent_date) ))) / 60) menit	
                FROM
                tracking_status_invoice ts
                LEFT JOIN workflow wk ON wk.workflow_id=ts.workflow_id
                LEFT JOIN invoice inv ON inv.id=ts.invoice_id 
                WHERE
                ts.user_nip = ?
                AND ( ts.received_date IS NULL OR ts.submited_date IS NULL)
                ORDER BY ts.id DESC
                ",[$nip]);



            
        if(count($info_pr)>0 OR count($info_po)>0 OR  count($info_gr)>0 OR  count($info_invoice)>0){
            if(count($info_pr)>0){
                foreach ($info_pr as $key) {
                    /*$receipt = $key->received_date?$key->received_date:$key->sent_date;
                    //$receipt = "2020-10-20 10:10:10";
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $receipt);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);*/
                    $sisa_waktu = (int) ($key->days)-$key->baku_waktu;
                   
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = $key->label;
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#ffd700";//kuning
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//merah
                    }
                    $info_terkini[] = array(
                        'id'        => $key->id,
                        'num'       => $key->num,
                        'doc'       => $doc,
                        'sent_date'       => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'sisa_waktu'      => $sisa_waktu,
                        'label'           => $label,
                        'warna'           => $color,
                    );
                    # code...
                }
            }

            if(count($info_po)>0){
                foreach ($info_po as $key) {
                    /*$receipt = $key->received_date?$key->received_date:$key->sent_date;
                    //$receipt = "2020-10-20 10:10:10";
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $receipt);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);*/
                    $sisa_waktu = (int) ($key->days)-$key->baku_waktu;
                   
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = $key->label;
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#ffd700";//kuning
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//merah
                    }
                    $info_terkini[] = array(
                        'id'        => $key->id,
                        'num'       => $key->num,
                        'doc'       => $doc,
                        'sent_date'       => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'sisa_waktu'      => $sisa_waktu,
                        'label'           => $label,
                        'warna'           => $color,
                    );
                    # code...
                }
            }

            if(count($info_gr)>0){
                foreach ($info_gr as $key) {
                    /*$receipt = $key->received_date?$key->received_date:$key->sent_date;
                    //$receipt = "2020-10-20 10:10:10";
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $receipt);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);*/
                    $sisa_waktu = (int) ($key->days)-$key->baku_waktu;
                   
                    $doc = strtoupper($key->doc);
                    if($sisa_waktu<0){
                        $label  = "On Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#ffd700";//kuning
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//merah
                    }
                    $info_terkini[] = array(
                        'id'        => $key->id,
                        'num'       => $key->num,
                        'doc'       => $doc,
                        'sent_date'       => $key->sent_date?date('d M Y',strtotime($key->sent_date)):"",
                        'sisa_waktu'      => $sisa_waktu,
                        'label'           => $label,
                        'warna'           => $color,
                    );
                    # code...
                }
            }

            if(count($info_invoice)>0){
                foreach ($info_invoice as $key) {
                    $receipt = $key->received_date?$key->received_date:$key->sent_date;
                    //$receipt = "2020-10-20 10:10:10";
                    $submit = $key->submited_date?$key->submited_date:date('Y-m-d H:s:i');
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $receipt);
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $submit);
                    $diff_in_days = $to->diffInDays($from);
                    $sisa_waktu = (int) ($diff_in_days+1)-$key->baku_waktu;
                   
                    $doc = strtoupper($key->source_doc);
                    if($sisa_waktu<0){
                        $label  = "On Progres";
                        $color  = "#0089C7";//biru
                    }else if($sisa_waktu==0){
                        $label  = "Tersisa 1 hari";
                        $color  = "#ffa500";//kuning
                    }else{
                        $label  = "Lewat ".$sisa_waktu." hari";
                        $color  = "#dc143c";//merah
                    }
                    $info_terkini[] = array(
                        'id'        => $key->source_id,
                        'num'       => $key->num,
                        'doc'       => $doc,
                        'sent_date'       => $receipt,
                        //'submit'       => $submit,
                        //'waktu'           => $diff_in_days+1,
                        'sisa_waktu'      => $sisa_waktu,
                        'label'           => $label,
                        'warna'           => $color,
                    );
                    # code...
                }
            }
            
        }else{
            $info_terkini = [];
        }

        if(count($dashboard)>0){
            $dashboard = $dashboard[0];
            $total_ = $dashboard->total;
            $wappr_ = $dashboard->wr_pr+$dashboard->wr_po+$dashboard->wr_gr;
            $inprogress_ = $dashboard->rec_pr+$dashboard->rec_po+$dashboard->rec_gr;
            $approved_ = $dashboard->approved;
        }else{
            $total_ = 0;
            $wappr_ = 0;
            $inprogress_ = 0;
            $approved_ = 0;
        }

        if(count($dashboard_invoice)>0){
            $total_inv = $dashboard_invoice[0]->total;
            $wappr_inv = $dashboard_invoice[0]->wappr;
            $inprogress_inv = $dashboard_invoice[0]->inprogress;
            $approved_inv = $dashboard_invoice[0]->approved;
        }else{
            $total_inv = 0;
            $wappr_inv = 0;
            $inprogress_inv = 0;
            $approved_inv = 0;
        }
        
        $return = array(
            'total' => $total_+$total_inv,
            'waiting_approval' => $wappr_+$wappr_inv,
            'inprogress' => @$inprogress_+$inprogress_inv,
            'approved' => @$approved_+$approved_inv,
            'date' => date('Y-m-d H:i:s'),
            'unread_inbox' => $this->unreadTotal(),
            'info_terkini' => $info_terkini
        );

        return $return;
        
    }

    


    

}

?>