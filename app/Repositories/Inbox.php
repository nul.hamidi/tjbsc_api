<?php
namespace App\Repositories;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use App\Model\InboxModel;
use App\Repositories\Api;

class Inbox extends Api{
    
    public function getInboxList($page,$req) {
        $return = array();
        $limit = @$req->limit?@$req->limit:20;
        $search = @$req->search?@$req->search:"";
        $user = auth()->user();
        $result = InboxModel::
                select('id','title','message','is_read','created_date')
                ->where('receipt_by',$user->nip)
                ->paginate($limit,$page);
            //->toSql();
        $result->map(function($res){
            if($res->is_read=='1'){
                $warna = "#ff8c00";//merah
                $read  = true;
            }
            else{
                $warna = "#1cac78";//merah
                $read  = false;
            }
           
            $res['title']               = $res->title?$res->title:"";
            $res['message']             = $res->message?$res->message:"";
            $res['date']                = $res->created_date? date('H:i, d/m/Y',strtotime($res->created_date)):"";
            $res['from']                = "Admin TJBSC";
            $res['read']                = $read;
            $res['read_color']          = $warna;
            
        
            return $res;
        });
        
        $data = array(
            'data'      => $result['data'],
           
        );
        return $result;
    }

    public function unreadTotal() {
        $return = array();
        $user = auth()->user();
        $result = InboxModel::
                select('title','message','is_read','created_date')
                ->where('is_read','0')
                //->where('receipt_by',$user->nip)
                ->count();
            //->toSql();
        
        return $result;
    }

    public function readMessage($message_id) {
        $return = array();
        $user = auth()->user();
        //return $message_id;
        
        
        $update = DB::table('inbox')
                    ->where('id',$message_id)
                    ->update(['is_read'=>'1']);
        
        return $update;
    }

    public function readAll() {
        $return = array();
        $user = auth()->user();
        //return $message_id;
        
        
        $update = DB::table('inbox')
                    ->update(['is_read'=>'1']);
        
        return $update;
    }

    



    

}

?>