<?php

namespace App\Providers;

use App\Model\SysUser;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            
            if(strlen($request->header('token'))>30){
                $token = $request->header('token');
            }
            else if(strlen($request->bearerToken())>30){
                $token = $request->bearerToken();
            }else{
                $token = $request->input('token');
            }
            if ($token) {
                return SysUser::where('remember_token', $token)->first();
            }
        });
    }
}
