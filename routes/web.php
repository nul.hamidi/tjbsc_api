<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Illuminate\Support\Str;
date_default_timezone_set('Asia/Jakarta');
$router->get('/', function () use ($router) {
    return "API TJB SC Versi 1.0";
});
$router->get('/api/version', function () use ($router) {
    return $router->app->version();
});
$router->get('/api/generate', function () use ($router) {
    //return str::random(32);
    $auth ="112020@tjbsc2020";
    return base64_encode($auth);
    /*10.47.0.20 TJBSC
    119.252.163.243:80 TJBSC Public
    10.47.0.31 maximo PLN (live)
    10.47.0.33 maximo PLN Dev
    10.14.9.39 Maximo TJBPS Live
    10.14.9.37 maximo tjbps Dev*/
});

$router->get('/api/maxauth', function () use ($router) {
    //return str::random(32);
    $auth ="112020@tjbsc2020";
    $user = "8309375Z:8309375Z";
    $srm ="6693107Z:6693107Z";
    $aeren ="8510219Z:8510219Z";
    $msbophar ="8002021A2:8002021A2";
    return base64_encode($msbophar);
    //return base64_decode("ODUxMDIxOVo6ODUxMDIxOVo=");
});

$router->get('/api/test-data', 'ExampleController@getPr');
$router->get('/api/read-api', 'ExampleController@readApipr');


$router->post('/api/register', 'AuthController@register');
$router->post('/api/login', 'AuthController@loginMaximo');
$router->post('/api/authmax', 'AuthController@loginMaximo');

$router->post('/api/pr/create', 'PrController@createMaximo');
$router->put('/api/pr/update', 'PrController@updateMaximo');
$router->post('/api/pr/submit', 'PrController@submitMaximo');
$router->get('/api/pr/list', 'PrController@listPRMaximo');

$router->post('/api/po/create', 'POController@createMaximo');
$router->put('/api/po/update', 'POController@updateMaximo');
$router->post('/api/po/submit', 'POController@submitMaximo');
$router->get('/api/po/list', 'POController@listPOMaximo');

$router->post('/api/gr/create', 'GoodsReceiptController@createMaximo');
$router->post('/api/gr/submit', 'GoodsReceiptController@submitMaximo');

//define middleware 'auth'
$router->group(
    ['prefix' => 'api','middleware' =>'auth'], 
     function() use ($router) {
        $router->get('/test-data-auth', 'ExampleController@getPr');
        //change password
        $router->put('/changepassword', 'AuthController@changePassword');
        //user
       $router->get('/users', 'UserController@userList');
       $router->get('/user-profile', 'UserController@userProfile');
       $router->get('/user/role', 'UserController@userRole');

       //scan qrcode
        $router->get('/qrcode', 'QrCodeController@scanNumQrCode');
        $router->get('/scanqrcode', 'QrCodeController@scanNewQrCode');
        $router->post('/hitqrcode', 'QrCodeController@hitQrCode');

       //home 
       $router->get('/home', 'HomeController@homeDashboard');
       // menu document
       $router->get('/pr', 'PrController@purchaseRequests');
       $router->get('/po', 'POController@purchaseOrder');
       $router->get('/gr', 'GoodsReceiptController@goodsReceipt');
       $router->get('/invoice', 'InvoiceController@invoice');

       //$router->get('/qrcode', 'QrCodeController@scanQrCode');
       $router->post('/received', 'QrCodeController@received');
       $router->post('/submit', 'QrCodeController@submit');
       $router->post('/submit-staff', 'QrCodeController@submitStaff');
       $router->post('/close', 'QrCodeController@close');

       $router->get('/history/ongoing', 'HistoryController@ongoing');
       $router->get('/history/pr', 'HistoryController@historyPR');
       $router->get('/history/po', 'HistoryController@historyPO');
       $router->get('/history/gr', 'HistoryController@historyGR');
       $router->get('/history/invoice', 'HistoryController@historyInvoice');
       $router->get('/history/traceability', 'HistoryController@historyTraceability');

       $router->get('/inboxs', 'InboxController@inboxList');
       $router->get('/inbox/unread', 'InboxController@unreadMessage');
       $router->post('/inbox/read-message', 'InboxController@viewMessage');
       $router->post('/inbox/read-all-message', 'InboxController@readAllMessage');
       
       $router->post('/inbox/read-all-message', 'InboxController@readAllMessage');
    }
);


$router->group(
    ['prefix' => 'api/test',], 
     function() use ($router) {
        $router->get('/pr-maximo', 'ExampleController@readApiMaximo');
        $router->get('/pr-pln', 'ExampleController@readApiMaximoPLN');
        $router->get('/user-tjbps', 'ExampleController@userTjbps');
        $router->get('/submit', 'ExampleController@testSubmit');
        
     }
    );
